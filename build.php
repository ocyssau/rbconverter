<?php

function removeDir( $path, $recursive = false, $verbose = false ){
	// Add trailing slash to $path if one is not there
	if (substr($path, -1, 1) != "/"){
		$path .= "/";
	}
	if($curdir = opendir($path)) {
		while($file = readdir($curdir)) {
			if($file != '.' && $file != '..') {
				$file = $path . $file;
				if (is_file($file) === true){
					if (!unlink($file)){
						echo "failed to removed File: " . $file . "\n\r"; return false;
					}else {if($verbose) echo "Removed File: " . $file . "\n\r";
					}
				}
				else if (is_dir($file) === true && $recursive === true){
					// If this Directory contains a Subdirectory and if recursivity is requiered, run this Function on it
					if (!removeDir($file, $recursive, $verbose)){
						if($verbose) echo "failed to removed File: " . $file . "\n\r";
						return false;
					}
				}

			}
		}
		closedir($curdir);
	}

	// Remove Directory once Files have been removed (If Exists)
	if(is_dir($path)){
		if(@rmdir($path)){
			if($verbose) echo "Removed Directory: " . $path . "\n\r";
			return true;
		}
	}
	return false;
}


function dircopy($srcdir, $dstdir, $verbose = false, $initial=true, $recursive=true, $mode=0755){
	if(!is_dir($dstdir)) mkdir($dstdir, $mode, true);
	if($curdir = opendir($srcdir)) {
		while($file = readdir($curdir)) {
			if($file == '.' || $file == '..'
					|| $file == 'jsdev' || $file == 'experimental'
			) continue;
				
			$srcfile = "$srcdir" . '/' . "$file";
			$dstfile = "$dstdir" . '/' . "$file";
			if(is_file("$srcfile")) {
				if($verbose) echo "Copying '$srcfile' to '$dstfile'...";
				if(copy("$srcfile", "$dstfile")) {
					touch("$dstfile", filemtime("$srcfile"));
					if($verbose) echo "OK\n";
				}
				else {print "Error: File '$srcfile' could not be copied!\n"; return false;
				}
			}
			else{
				if(is_dir("$srcfile") && $recursive){
					dircopy("$srcfile", "$dstfile", $verbose, false);
				}
			}
		}
		closedir($curdir);
	}
	return true;
}


function cleansvn($dir, $verbose = true){
	if(!is_dir($dir)) return false;
	if($curdir = opendir($dir)) {
		while( false !== ($file = readdir($curdir)) ) {
			if($file == '.' || $file == '..') continue;
			if($file == '.svn' || $file == 'Thumbs.db') {
				$file = $dir . '/' . $file;
				if(is_dir("$file")) {
					if( removeDir($file, true, true) ){
						echo "Delete directory $file \n\r";
					}else {
						print "Error: $file cant be suppressed \n";
					}
				}
			}else{
				$file = $dir . '/' . $file;
				if( is_dir($file) ) {
					cleansvn($file, $verbose);
				}
			}
		}
		closedir($curdir);
	}

	return true;
}


function emptyDir( $dir , $verbose = true){
	if($curdir = opendir( $dir ))
		while($file = readdir($curdir)) {
		if($file == '.' || $file == '..' || $file == 'readme' ) continue;
		$file = $dir . '/' . $file;
		if (is_file($file) === true){
			if (!unlink($file)){
				echo "failed to removed File: " . $file . "\n\r"; return false;
			}else {if($verbose) echo "Removed File: " . $file . "\n\r";
			}
		}
		if (is_dir($file) === true){
			if (!removeDir( $file, true, true )){
				echo "failed to removed directory: " . $file . "\n\r"; return false;
			}else {if($verbose) echo "Removed directory: " . $file . "\n\r";
			}
		}
	}
	closedir($curdir);
	return true;
}

/**
 * @todo : add original directory structure
 *
 * @param String $dir
 * @param String $zipfile
 * @param Boolean $recursive
 * @param Boolean $verbose
 */
function zipDir( $dir , ZipArchive $zip, $recursive=true, $verbose = true){
	if($curdir = opendir( $dir ))
		while($localfile = readdir($curdir)) {
		if($localfile == '.' || $localfile == '..' ) continue;
		if($dir == './' || $dir == '.' ) $file = $localfile;
		else $file = $file = $dir . '/' . $localfile;
		if (is_file($file) === true){
			$zip->addFile($file, $file);
			if($verbose) echo "Add File: $file to zip file \n\r";
		}
		if (is_dir($file) === true){
			zipDir( $file , $zip, $recursive, $verbose);
		}
	}
	closedir($curdir);
	return true;
}


function build($tmpdir, $svnversion, $pathToZend, $verbose = false){
	//Create build directory
	$sourceDir = realpath ( dirname ( __FILE__ ) );
	require_once($sourceDir . '/server/library/Rbcs/Rbcs.php');

	$version = Rbcs::getVersion(); //array
	$build = time();
	$buildName = 'rbconverter_' . $version['version'] . '_build' . $build;
	$buildDir1 = $tmpdir . '/' . $buildName;
	$buildDir = $tmpdir . '/' . $buildName . '/rbconverter';

	//Copier les donn�es
	mkdir($buildDir, 0755, true);

	if(!dircopy($sourceDir, $buildDir, $verbose)) die("ne peut pas copier $sourceDir vers $buildDir");

	//clean svn
	cleansvn($buildDir);

	if( is_file($buildDir . '/build.bat') )
		unlink( $buildDir . '/build.bat' );

	if( is_file($buildDir . '/build.php') )
		unlink( $buildDir . '/build.php' );

	if( is_dir($buildDir . '/.settings') )
		removeDir( $buildDir . '/.settings', true, $verbose );

	if( is_file($buildDir . '/.buildpath') )
		unlink( $buildDir . '/.buildpath' );

	if( is_file($buildDir . '/.project') )
		unlink( $buildDir . '/.project' );

	if( is_file($buildDir . '/.zfproject.xml') )
		unlink( $buildDir . '/.zfproject.xml' );

	if( is_file($buildDir . '/build.log') )
		unlink( $buildDir . '/build.log' );

	if( is_file($buildDir . '/docs/DocRBConverter_dev_api.odt') )
		unlink( $buildDir . '/docs/DocRBConverter_dev_api.odt' );

	if( is_file($buildDir . '/docs/DocRBConverter_install.odt') )
		unlink( $buildDir . '/docs/DocRBConverter_install.odt' );

	//Set the build number
	$class_rbcs_file = $buildDir . '/server/library/Rbcs/Rbcs.php';
	$class_rbcs_content = file_get_contents( $class_rbcs_file );
	$class_rbcs_content = str_replace('%RBCS_BUILD%', $build, $class_rbcs_content);
	$class_rbcs_content = str_replace('%RBCS_SVN_VERSION%', $svnversion, $class_rbcs_content);
	file_put_contents($class_rbcs_file, $class_rbcs_content);


	//Copy zend librairy
	if(!dircopy($pathToZend, $buildDir . '/external/Zend', $verbose))
		echo ("ne peut pas copier $pathToZend vers $buildDir/external/");

	//Generate zip file
	$zipArchive = new ZipArchive();
	$zipfile = $tmpdir . '/' . $buildName . '.zip';
	if($zipArchive->open($zipfile, ZIPARCHIVE::CREATE) !== true ){
		die("Impossible de cr�er le zip $zipfile");
	}
	chdir( $buildDir . '/..' );
	zipDir( 'rbconverter/client' , $zipArchive, true, $verbose);
	zipDir( 'rbconverter/docs' , $zipArchive, true, $verbose);
	zipDir( 'rbconverter/external' , $zipArchive, true, $verbose);
	zipDir( 'rbconverter/samples' , $zipArchive, true, $verbose);
	zipDir( 'rbconverter/server' , $zipArchive, true, $verbose);
	$zipArchive->addFromString('rbconverter/version.ini', $version['version'] . "\n" );
	$zipArchive->close();

	//clean the temp
	chdir($tmpdir);
	removeDir( $buildDir1, true, $verbose );

}

$verbose = true;
$svnversion = 7;
$pathToZend = 'C:/htdocs/ZendFramework-1.10.1/library/Zend';
build( 'c:/tmp' , $svnversion , $pathToZend, $verbose);

