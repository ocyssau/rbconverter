'first try to kill exisiting process
KillProc("rbconverter.exe")

Sub KillProc( myProcess )
	Dim blnRunning, colProcesses, objProcess

	blnRunning = False

	Set colProcesses = GetObject("winmgmts:{impersonationLevel=impersonate}").ExecQuery( "Select * From Win32_Process", , 48 )

	For Each objProcess in colProcesses
		If LCase( myProcess ) = LCase( objProcess.Name ) Then
			' Confirm that the process was actually running 
			blnRunning = True 
			' Get exact case for the actual process name
			myProcess = objProcess.Name
			' Kill all instances of the process
			objProcess.Terminate()
		End If
	Next
End Sub