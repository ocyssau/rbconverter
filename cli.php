#!/usr/bin/php
<?php

namespace rbconverter\cli;

use Rbcs;

/**
 */
function optionh() {
	$display = 'Utilisation :

	php tests.php
	[-t]
	[-i]
	[-h]

	Avec:

	-i
	: initialise les donn�es de test.

	-h
	: Affiche cette aide.

	-t : Execute la function test <fonction> parmis les fonctions suivantes :';
	echo $display;
	displaylisttest ();
}
function displaylisttest() {
	echo $display = '';
}

/**
 */
function run() {
	chdir ( __DIR__ );
	// var_dump(getcwd());die;
	
	error_reporting ( E_ALL ^ E_DEPRECATED ^ E_NOTICE ^ E_STRICT );
	ini_set ( 'display_errors', 1 );
	ini_set ( 'display_startup_errors', 1 );
	
	ini_set ( 'xdebug.collect_assignments', 1 );
	ini_set ( 'xdebug.collect_params', 4 );
	ini_set ( 'xdebug.collect_return', 1 );
	ini_set ( 'xdebug.collect_vars', 1 );
	ini_set ( 'xdebug.var_display_max_children', 1000 );
	ini_set ( 'xdebug.var_display_max_data', 5000 );
	ini_set ( 'xdebug.var_display_max_depth', 10 );
	
	/*
	 * ASSERT_ACTIVE assert.active 1 active l'évaluation de la fonction assert()
	 * ASSERT_WARNING assert.warning 1 génére une alerte PHP pour chaque assertion fausse
	 * ASSERT_BAIL assert.bail 0 termine l'exécution en cas d'assertion fausse
	 * ASSERT_QUIET_EVAL assert.quiet_eval 0 désactive le rapport d'erreur durant l'évaluation d'une assertion
	 * ASSERT_CALLBACK assert.callback (NULL) fonction de rappel utilisateur, pour le traitement des assertions fausses
	 */
	
	assert_options ( ASSERT_ACTIVE, 1 );
	assert_options ( ASSERT_WARNING, 1 );
	assert_options ( ASSERT_BAIL, 0 );
	// assert_options( ASSERT_CALLBACK , 'myAssertCallback');
	
	/*
	 * ob_start();
	 * phpinfo();
	 * file_put_contents('.phpinfos.out.txt', ob_get_contents());
	 */
	
	echo 'include path: ' . get_include_path () . PHP_EOL;
	
	// $zver=new \Zend\Version\Version();
	// echo 'ZEND VERSION : ' . $zver::VERSION . "\n";
	
	$shortopts = '';
	$shortopts .= "h"; // affiche l'aide
	$shortopts .= "t:"; // call test function <function>
	$shortopts .= "i"; // initialise les données de test
	$shortopts .= "c:"; // La classe <class> de test a lancer
	$shortopts .= "m:"; // La methode <method> a executer
	
	$longopts = array ();
	
	$norun = array (
			'nodao',
			'm',
			'p' 
	);
	$options = getopt ( $shortopts, $longopts );
	foreach ( array_keys($options) as $o ){
		if (!in_array($o,$norun)){
			call_user_func( __NAMESPACE__ . '\option' . $o, $options );
		}
	}
	return;
}

/**
 * execute la fonction test sp�cifi�e
 */
function optiont($options) {
	// initialize
	boot ();
	$suffix = $options ['t'];
	if ($suffix == '') {
		$suffix = 'my';
	}
	
	call_user_func ( __NAMESPACE__ . '\test_' . $suffix, array () );
}

/**
 * Initialize
 */
function boot() {
	require 'server/module/Rbcs/Rbcs.php';
	include 'server/boot.php';
	
	$rbcs = new Rbcs ();
	$global = include 'server/config/autoload/global.php';
	$local = include 'server/config/autoload/local.php';
	$rbcs->setConfig( array_merge ( $global, $local ) );
	
	rbinit_autoloader();
	rbinit_rbservice();
	rbinit_web();
	rbinit_enableDebug(true);
}

function test_OooConverter() {
	$controllerClass = '\Rbcs\Controller\RbgateController';
	$controller = new $controllerClass();
	
	$files = array(
			'test.doc',
			'test.CATPart' 
	);
	$_POST ['files'] = $files;
	
	$controller->preDispatch();
	$controller->fromfilesService();
	$controller->postDispatch();
}

function test_DrawingConverter() {
	$controllerClass = '\Rbcs\Controller\RbgateController';
	$controller = new $controllerClass();
	$files = array(
		'test.CATDrawing',
	);
	$_GET ['files'] = $files;
	$controller->preDispatch();
	$controller->fromfilesService();
	$controller->postDispatch();
}

function test_PartConverter() {
	$controllerClass = '\Rbcs\Controller\RbgateController';
	$controller = new $controllerClass();
	$files = array(
		'test.CATPart',
	);
	$_GET ['files'] = $files;
	$controller->preDispatch();
	$controller->fromfilesService();
	$controller->postDispatch();
}

function test_ProductConverter() {
	$controllerClass = '\Rbcs\Controller\RbgateController';
	$controller = new $controllerClass();
	
	$files = array(
		'Jouet_Helicopter.CATProduct'
	);
	$_GET ['files'] = $files;
	$controller->preDispatch();
	$controller->fromfilesService();
	$controller->postDispatch();
}

run();
