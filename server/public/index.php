<?php
header('Access-Control-Allow-Origin: *');
/*
 * Controller for ranchbe 0.6
 * respond to url like /<module>/<controller>/<action>
 *
 * <module> is a sub-directory of Controller directory
 *
 * <controller> is class name define in file <controller>.php.
 * This class must be set in namespace <controller>
 *
 * <action> is a method of <controller>
 */
chdir(__DIR__ . '/../../.');
// var_dump(getcwd());die;

ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);

if ( !defined('APPLICATION_PATH') ) {
	define('APPLICATION_PATH', realpath(__DIR__ . '/../'));
}

/* Setup autoloading */
require 'server/module/Application/src/Application/Application.php';
require 'server/module/Rbcs/Rbcs.php';
include 'server/boot.php';

$rbcs = new Rbcs();
$local = include 'server/config/autoload/local.php';
$global = include 'server/config/autoload/global.php';
$rbcs->setConfig(array_merge($global, $local));
rbinit_autoloader();
rbinit_rbservice();
rbinit_web();
rbinit_enableDebug(true);
rbinit_converter($rbcs->getConfig());

$rbcs->predispatch()->run();
