$(document).on({
	ajaxStart: function(){
		$('#rbloadwaiting').modal('show');
	},
	ajaxStop: function(){
		$('#rbloadwaiting').modal('hide');
	}
});

$(function(){
	$("input.switcher").click(function(){
		var state = $(this).prop("checked");
		var selector = $(this).data("toswitch");
		//$(selector).prop("checked", state);
		rbMarkRows($(selector), state);
	});

	$( ".datepicker" ).datepicker({
		showWeek: true,
		dateFormat:"dd-mm-yy",
		firstDay: 1
	});

	$( ".datepicker" ).datepicker( "option", $.datepicker.regional[ "fr" ] );

	$(".rb-feedbacks").alert();

	$(".rb-popup").click(function(e){
		e.preventDefault();
		var width = $(this).data("width");
		var height = $(this).data("height");
		var url = $(this).prop("href");
		var target = $(this).prop("title");
		popupP(url, target , height , width)
		return false;
	});

	$(".rb-openintop").click(function(e){
		e.preventDefault();
		var href = $(this).attr("href");
		//the main window must be set with the name defined here by set name as 'window.name="main rb window";'
		var mainWindow = window.open(href, "main rb window");
		mainWindow.focus();
		return false;
	});

	rbMarkRowsInit();
});

function initSortableHeader(orderby, order, url)
{
	var th = $('[data-field="'+orderby+'"]');
	th.data('order', order);
	if(order=='asc'){
		th.children('span').addClass("glyphicon-chevron-down");
	}
	else{
		th.children('span').addClass("glyphicon-chevron-up");
	}

	$('.sortable').click(function(e){
		var order = $(this).data('order');
		var orderby = $(this).data('field');
		if(order == 'asc'){
			order='desc';
		}
		else{
			order='asc';
		}

		var orderQuery = 'order='+order+'&orderby='+orderby;
		if (url.search(/(\?)./i) == -1) {
			url = url+'?'+orderQuery;
		}
		else{
			url = url+'&'+orderQuery;
		}
		window.open(url,"_self");
	});
}

/**
 * 
 * @param selector
 */
function initEmbededContextMenu(selector)
{
	$(selector)
	.button({
		text: true,
	})
	.click(function(e) {
		var menu = $( this ).parent().next().toggle().position({
			my: "left top",
			at: "left bottom",
			of: this
		});
		$( document ).one( "click", function(e) {
			menu.toggle();
		});
		return false;
	})
	.parent()
	.buttonset()
	.next()
	.hide()
	.menu();
}

/**
 * @param selector
 */
function initContextMenu(selector, menuDefinitionSelector, actionCallback)
{
	var button = $(selector);
	var menuDefinition = $(menuDefinitionSelector);

	button.button({
		text: true,
	})
	.click(function(e) {
		menuDefinition.data('menuItemData', $(this).data());

		//console.log($(this).data('acode'));
		var button = $(this);

		(menuDefinition.find('li')).each(function(i, elemt){
			var assert = $(elemt).data('assert');
			if(assert){
				var display = eval(assert);
			}
			else{
				var display = true;
			}

			//console.log(button.data('lockbyid'),button.data('currentuserid'),button.data('id'));

			if(display==true){
				$(elemt).show();
			}
			else{
				$(elemt).hide();
			}
		});

		var menu = menuDefinition.toggle().position({
			my: "left top",
			at: "left bottom",
			of: this
		});

		/* when click out, hide menu */
		$( document ).one( "click", function(e) {
			menu.toggle();
		});

		return false;
	})

	.parent().buttonset();

	/* Set action callback on each <a>*/
	menuDefinition.find('a').click(actionCallback);
	menuDefinition.hide().menu();
}

/**
 * @returns {___anonymous671_715}
 */
function getUrlWithoutSort()
{
	//Url without params
	var url = window.location.origin + window.location.pathname;
	var output = {
			order:null,
			orderby:null,
			url:url
	};

	//put url paramters in object parameters without order and orderby
	if (window.location.search.length > 1) {
		var aCouples = window.location.search.substr(1).split('&');
		var aItKey, nKeyId;
		for (nKeyId = 0; nKeyId < aCouples.length; nKeyId++) {
			aItKey = aCouples[nKeyId].split('=');
			if (aItKey[0] == 'order') {
				output.order=aItKey[1];
			}
			else if (aItKey[0] == 'orderby') {
				output.orderby=aItKey[1];
			}
			else{
				if(nKeyId>0){
					url = url + "&" + aCouples[nKeyId];
				}
				else{
					url = url + "?" + aCouples[nKeyId];
				}
			}
		}
	}
	output.url = url;
	return output;
}

/**
 *  Fonction pour afficher une fenetre pop-up dont la taille est parametrable 
 */
function popupP(pageUrl, windowName , height , width)
{
	var option = "height=" + height + ", width=" + width + ", toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no";
	if(pageUrl.indexOf("?") > 0){
		pageUrl=pageUrl+'&layout=popup';
	}
	else{
		pageUrl=pageUrl+'?layout=popup';
	}

	/* put index to end */
	var urlIndex = pageUrl.split('#');

	if (typeof urlIndex[1]!=='undefined'){
		pageUrl = urlIndex[0];
		var urlIndex = urlIndex[1].split('&');
		pageUrl = pageUrl+'&'+urlIndex[1];
		var urlIndex = urlIndex[0];
		pageUrl=pageUrl+'#'+urlIndex;
	}

	window.open(pageUrl, windowName, config=option);
}

//globale function to generate uniqid
function uniqid() {
	var ts = String(new Date().getTime());
	return ('id'+ts);
}

//global function to be use in callback
function ajaxErrorToMsg(data)
{
	//var reponsejson = JSON.parse(data.responseText);
	//var msg = "A error is occured during this operation with message : \n" + reponsejson.error.message + ' Code:' + reponsejson.error.code;
	console.log(data);
	//alert(msg);
};

/**
 *
 */
function rbMarkRowsInit() 
{
	$("td.selectable").click(function(){
		var tr = $(this).parent("tr");
		var checkbox = tr.find(":checkbox");
		if(tr.hasClass("marked")){
			checkbox.prop("checked", false);
		}
		else{
			checkbox.prop("checked", true);
		}
		tr.toggleClass("marked");

		var displaybox = $("#displaySelectedRowCount");
		displaybox.html(rbCountSelectedRows());
	});

	return false;
}

/**
 *
 */
function rbMarkRows(checkboxes, on)
{
	checkboxes.each(function(index){
		var checkbox = $(this);
		var tr = checkbox.parents("tr");
		if(on==true){
			tr.addClass("marked");
			checkbox.prop("checked", true);
		}
		else{
			tr.removeClass("marked");
			checkbox.prop("checked", false);
		}
	});

	var displaybox = $("#displaySelectedRowCount");
	displaybox.html(rbCountSelectedRows());
	return false;
}

/**
 * Display the count of row selected by PMA_markRowsInit
 */
function rbCountSelectedRows()
{
	// for every table row ...
	var count = 0;
	var rows = $('tr');
	rows.each(function(index){
		var checkbox = $(this).find(":checkbox");
		if(checkbox.prop("checked") == true){
			count++;
		}
	});
	return count;
}

/**
 * 
 */
function confirmDialogBox(title, message, continueCallback) {
	var id = 'rb-dialog-confirm';
	if($("#"+id).length == 0){
		$("body").append('<div id="'+id+'"></div>');
	}
	var confirmDialog = $("#"+id);
	confirmDialog.attr('title', title);
	confirmDialog.html(message);

	confirmDialog.dialog({
		resizable: true,
		modal: true,
		buttons: {
			"Ignore And Continue": function() {
				continueCallback();
				$(this).dialog("close");
			},
			"Cancel": function() {
				$(this).dialog("close");
			}
		}
	});
};

