<?php
// %LICENCE_HEADER%

/**
 * Init constants for web interface
 *
 * @return void
 */
function rbinit_web()
{
	// Disable auto register global of sessions variables.
	// Note that register_globals must be set to false too.
	ini_set('session.bug_compat_42', false);
	
	define('CRLF', "<br />");
	
	if ( getenv('BASEURL') ) {
		define('BASEURL', getenv('BASEURL'));
	}
	else {
		define('BASEURL', '');
	}
}

/**
 */
function rbinit_rbservice()
{
	/* pour changer tous les messages d'erreurs en ErrorException */
	set_error_handler("rbinit_exception_error_handler", E_ERROR | E_CORE_ERROR | E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_USER_ERROR);
}

/**
 *
 * @param integer $errno        	
 * @param string $errstr        	
 * @param string $errfile        	
 * @param integer $errline        	
 * @throws ErrorException
 */
function rbinit_exception_error_handler($errno, $errstr, $errfile, $errline)
{
	throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}

/**
 * Init constants for command line interface
 */
function rbinit_cli()
{
	if ( stristr($_SERVER['HTTP_USER_AGENT'], 'windows') ) {
		define('CLIENT_OS', 'WINDOWS');
		define('PHP_EOL', '\r\n');
	}
	else if ( stristr($_SERVER['HTTP_USER_AGENT'], 'macintosh') || stristr($_SERVER['HTTP_USER_AGENT'], 'mac_powerpc') ) {
		define('CLIENT_OS', 'MACINTOSH');
		define('PHP_EOL', '\r');
	}
	else {
		define('CLIENT_OS', 'UNIX');
		define('PHP_EOL', '\n');
	}
	
	define('CRLF', PHP_EOL);
}

/**
 * Init a autoloader.
 * Not use in application, prefer Zend_Autoload.
 * If $zend = true, init a Zend_Loader_Autoloader
 * else add _rbinit_loader in spl_autoload_register()
 */
function rbinit_autoloader()
{
	$loader = include 'vendor/autoload.php';
	$loader->set('Application\\', 'module/Application/src');
	
	foreach( Rbcs::get()->getConfig('modules') as $module ) {
		$loader->set($module . '\\', 'server/module/' . $module . '/src');
	}
	
	return $loader;
}


/**
 * Init a autoloader.
 * Not use in application, prefer Zend_Autoload.
 * If $zend = true, init a Zend_Loader_Autoloader
 * else add _rbinit_loader in spl_autoload_register()
 */
function rbinit_converter($config)
{
	$map = $config['converter.map'];
	$factory = \Rbcs\Converter\Factory::get();
	$factory->setMap($map);
	return $factory;
}

/**
 */
function rbinit_enableDebug($enable = true)
{
	if ( $enable ) {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		
		ini_set('xdebug.show_local_vars', 1);
		ini_set('xdebug.dump_undefined', 1); // Display undefined var too
		                                     // ini_set('xdebug.dump.POST', '*'); //Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();
		                                     // ini_set('xdebug.dump.GET', '*'); //Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();
		
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.var_display_max_children', 1000);
		ini_set('xdebug.var_display_max_data', 5000);
		ini_set('xdebug.var_display_max_depth', 4);
		ini_set('xdebug.max_nesting_level', 100); // three nested levels of array elements and object relations are displayed
		
		ini_set('xdebug.show_exception_trace', 0);
		ini_set('xdebug.trace_format', 0); // 1: computer readable format / 0: shows a human readable indented trace file with: time index, memory usage, memory delta (if the setting xdebug.show_mem_delta is enabled), level, function name, function parameters (if the setting xdebug.collect_params is enabled, filename and line number.
	}
	else {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
		ini_set('display_errors', 0);
		ini_set('display_startup_errors', 1);
		ini_set('xdebug.show_local_vars', 0);
		ini_set('xdebug.dump_undefined', 0); // Display undefined var too
		ini_set('xdebug.dump.POST', '*'); // Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();
		ini_set('xdebug.dump.GET', '*'); // Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.var_display_max_children', 10);
		ini_set('xdebug.var_display_max_data', 50);
		ini_set('xdebug.var_display_max_depth', 3);
		ini_set('xdebug.max_nesting_level', 10); // three nested levels of array elements and object relations are displayed
		ini_set('xdebug.show_exception_trace', 0);
		ini_set('xdebug.trace_format', 0); // 1: computer readable format / 0: shows a human readable indented trace file with: time index, memory usage, memory delta (if the setting xdebug.show_mem_delta is enabled), level, function name, function parameters (if the setting xdebug.collect_params is enabled, filename and line number.
	}
}
