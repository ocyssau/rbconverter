<?php

/**
 *
 *
 */
class Rbcs extends \Application\Application
{

	/**
	 *
	 * @var Rbcs
	 */
	protected static $instance;

	/**
	 */
	public function __construct()
	{
		self::$instance = $this;
	}

	/**
	 *
	 * @return Rbcs
	 */
	public static function get()
	{
		if ( !isset(self::$instance) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}
