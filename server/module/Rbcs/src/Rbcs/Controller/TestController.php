<?php
namespace Rbcs\Controller;

use Rbcs\Converter\Factory;

/**
 *
 *        
 */
class TestController extends \Application\Controller\Controller
{

	/**
	 * 
	 */
	public function init()
	{
		$this->_helper->viewRenderer->setNoRender();
	}

	/**
	 * 
	 */
	public function indexAction()
	{
		echo 'Welcome to RbConverterServer. Visit <a href="http://www.ranchbe.com">www.ranchbe.com</a>';
	}

	/**
	 * 
	 */
	public function docpdfAction()
	{
		$from_file = 'data/samples/test.doc';
		$handle = fopen($from_file, "rb");
		$options['data'] = base64_encode(fread($handle, filesize($from_file)));
		$options['to_file'] = 'result2.pdf';
		$options['from_mtype'] = 'doc';
		$options['to_mtype'] = 'pdf';
		
		// Actif only if permit by configuration directive "transports"
		$options['return_by'] = 'soap';
		
		try {
			$converter = Factory::get()->getConverter('doc','pdf');
			$ret = $converter->convert($options);
		}
		catch( \SoapFault $e ) {
			print $e;
		}
		
		// $data = base64_decode($ret['data']);
		// $to_file = 'c:\tmp\toto.pdf';
		// $fp = fopen($to_file, "a");
		// fputs($fp, $data);
		
		if ( !$converter->getError() ) {
			echo "Conversion succesfull of file $from_file <br />";
			echo $ret['data'];
		}
		else {
			var_dump($ret);
			echo $converter->getError();
		}
	}

	/**
	 * 
	 */
	public function catiajpgAction()
	{
		$from_file = 'data/samples/test.CATPart';
		$handle = fopen($from_file, "rb");
		$options['data'] = base64_encode(fread($handle, filesize($from_file)));
		$options['to_file'] = 'result.jpg';
		$options['from_mtype'] = 'catpart';
		$options['to_mtype'] = 'jpg';
		$options['return_by'] = 'soap';
		
		$converter = Factory::get()->getConverter('catpart','jpg');
		$ret = $converter->convert($options);
		
		if ( !$converter->getError() ) {
			echo "Conversion succesfull of file $from_file <br />";
			echo $ret['data'];
		}
		else {
			var_dump($ret);
			echo $converter->getError();
		}
	}

	/**
	 * 
	 */
	public function wordcomAction()
	{
		
		/* Start Word */
		$word = new \COM("word.application") or die("Impossible d'instancier l'application Word");
		echo "Word started, version {$word->Version}\n";
		
		/* Put in front of screen */
		$word->Visible = 1;
		
		/* Create empty doc */
		$word->Documents->Add();
		
		/* some examples */
		$word->Selection->TypeText("Ceci est un test...");
		$word->Documents[1]->SaveAs("data/testcom.doc");
		
		/* Close */
		$word->Quit();
		$word = null;
	}
} /* End of class */
