<?php
namespace Rbcs\Controller;

use Rbcs;
use Zend\View\Model\ViewModel;

/**
 */
class ConfigureController extends \Application\Controller\Controller
{

	/* */
	protected $localConfigFile = 'server/config/autoload/local.php';

	/**
	 */
	public function indexAction()
	{
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] : $cancel = false;
		isset($_REQUEST['validate']) ? $validate = $_REQUEST['validate'] : $validate = false;
		
		if ( $cancel ) {
			return;
		}
		
		$rbcs = Rbcs::get();
		$view = new ViewModel();
		
		/* Form */
		$form = new \Rbcs\Form\Config\EditForm();
		
		/* Model */
		$config = new \Application\Model\Config($rbcs->getConfig());
		$form->bind($config);
		
		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->isValid() ) {
				/* Load local configuration */
				$gfile = $this->localConfigFile;
				if ( is_file($gfile) ) {
					$local = include $gfile;
				}
				else {
					$local = array();
				}
				
				/* Update local configuration */
				$local['converter.workingDir'] = $config->converter_workingDir;
				$local['wildspace.path'] = $config->wildspace_path;
				$local['converter.working.cleanup'] = (int)$config->converter_working_cleanup;
				$datas = "<?php \nreturn " . var_export($local, true) . ';';
				
				/* Write datas in local file */
				file_put_contents($gfile, $datas);
				
				return $this->successAction();
			}
		}
		
		$view->pageTitle = 'Edit Configuration';
		$view->form = $form;
		$view->setTemplate($form->template);
		
		return $view;
	}

	/**
	 */
	public function successAction()
	{
		$view = new ViewModel();
		$view->pageTitle = 'Configuration is saved';
		$view->localConfigFile = $this->localConfigFile;
		$view->setTemplate('rbcs/config/success');
		return $view;
	}
}
