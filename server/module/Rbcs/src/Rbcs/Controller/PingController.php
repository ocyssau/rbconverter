<?php
namespace Rbcs\Controller;

use Rbcs\Converter;
use Zend\View\Model\ViewModel;

/**
 */
class PingController extends \Application\Controller\Controller
{

	/**
	 *
	 * @var Converter\ConverterInterface
	 */
	public $converter;

	/**
	 *
	 * @var string
	 */
	public $workingDir;

	/**
	 *
	 * @var string
	 */
	public $wildspace;

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function indexAction()
	{
		return $this->partAction();
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function pingAction()
	{
		return $this->partAction();
	}

	/**
	 */
	public function pingService()
	{
		return $this->partService();
	}

	/**
	 */
	public function productService()
	{
		$json = file_get_contents('data/samples/exportCatproduct_exemple.txt');
		echo $json;
		die();
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function productAction()
	{
		$view = new ViewModel();
		$json = file_get_contents('data/samples/exportCatproduct_exemple.txt');
		$view->setTemplate('rbcs/rbgate/ping');
		$view->datas = $json;
		return $view;
	}

	/**
	 */
	public function partService()
	{
		$json = file_get_contents('data/samples/exportCatpart_exemple.txt');
		echo $json;
		die();
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function partAction()
	{
		$view = new ViewModel();
		$json = file_get_contents('data/samples/exportCatpart_exemple.txt');
		$view->setTemplate('rbcs/rbgate/ping');
		$view->datas = $json;
		return $view;
	}

	/**
	 */
	public function drawingService()
	{
		$json = file_get_contents('data/samples/exportCatdrawing_exemple.txt');
		echo $json;
		die();
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function drawingAction()
	{
		$view = new ViewModel();
		$json = file_get_contents('data/samples/exportCatdrawing_exemple.txt');
		$view->setTemplate('rbcs/rbgate/ping');
		$view->datas = $json;
		return $view;
	}

	/**
	 * fake service for dev only
	 */
	public function fromdatasService()
	{
		return $this->pingService();
	}

	/**
	 * fake service for dev only
	 */
	public function fromwsfilesService()
	{
		return $this->pingService();
	}
}
