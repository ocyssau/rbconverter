<?php
namespace Rbcs\Controller;

use Zend\View\Model\ViewModel;
use Rbcs\Catia;

/**
 */
class IndexController extends \Application\Controller\Controller
{

	/**
	 */
	public function indexService()
	{
		return array(
			'Index Service'
		);
	}

	/**
	 */
	public function indexAction()
	{
		$view = new ViewModel();
		$view->pageTitle = __METHOD__;
		$view->setTemplate('rbcs/index/index');
		return $view;
	}
	
	/**
	 */
	public function quitcatiaService()
	{
		$catia = new Catia\Application();
		$catia->connect();
		$catia->quit();
	}
	
	/**
	 */
	public function quitcatiaAction()
	{
		$catia = new Catia\Application();
		$catia->connect();
		$catia->quit();
	}
}
