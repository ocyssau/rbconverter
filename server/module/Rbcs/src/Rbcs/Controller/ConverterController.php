<?php
namespace Rbcs\Controller;

use Rbcs\Converter;
use Zend\View\Model\ViewModel;
use Rbcs\Converter\Factory as ConverterFactory;
use Application\Controller\ControllerException;

/**
 */
class ConverterController extends \Application\Controller\Controller
{

	/**
	 *
	 * @var Converter\ConverterInterface
	 */
	public $converter;

	/**
	 */
	public function indexAction()
	{
		$view = new ViewModel();
		$view->setTemplate('rbcs/rbgate/converter');
		
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] : $cancel = false;
		isset($_REQUEST['validate']) ? $validate = $_REQUEST['validate'] : $validate = false;
		
		if ( $cancel ) {
			return $this->redirectTo('/rbcs');
		}
		
		/* */
		$workingdir = $this->application->getConfig('converter.workingDir');
		$workingCleanup = $this->application->getConfig('converter.working.cleanup');
		
		/* Form */
		$form = new \Rbcs\Form\Rbgate\ConverterForm();
		
		/* Model */
		$formModel = new \Rbcs\Form\Rbgate\ConverterFormModel($workingdir);
		$form->bind($formModel);
		
		/* Try to validate the form */
		if ( $validate ) {
			/* Make certain to merge the files info! */
			$post = array_merge_recursive($_POST, $_FILES);
			$form->setData($post);
			if ( $form->isValid() ) {
				try{
					
					$converter = ConverterFactory::get()->getConverter($formModel->getFromtype(), $formModel->getTotype());
					$converter->workingDir($workingdir);
					
					$fromfile = $formModel->getFromFile();
					$tofile = $workingdir . '/' . $formModel->getToFile();
					
					$converter->fromFile($fromfile)->toFile($tofile)->convert();
					
					$this->download($tofile);
					
					if($workingCleanup){
						@unlink($fromfile);
						@unlink($tofile);
						$files = $converter->getResult()->getDatas()['files'];
						foreach($files as $file){
							if(is_file($file)){
								@unlink($file);
							}
						}
					}
					
					die();
				}
				catch(\Exception $e){
					throw new ControllerException($e->getMessage());
				}
			}
		}
		
		$view->pageTitle = 'Converter';
		$view->form = $form;
		
		return $view;
	}

	/**
	 *
	 * @param string $file
	 */
	protected function download($file)
	{
		header("Content-disposition: attachment; filename=\"$file\"");
		header("Content-Type: " . $mimeType);
		header("Content-Transfer-Encoding: \"$file\"\n"); // Surtout ne pas enlever le \n
		header("Content-Length: " . (filesize($file)));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		readfile($file);
	}

	/**
	 */
	public function feedbackAction($result)
	{
		$view = new ViewModel();
		$view->setTemplate('rbcs/rbgate/converter/feedback');
		
		$this->respons->setDatas('data1', $result->getDatas());
		$this->respons->setFeedbacks($result->getFeedback());
		$errors = $result->getErrors();
		if ( $errors ) {
			$this->respons->setErrors($errors);
		}
		
		$view->pageTitle = 'Converter feedbacks';
		$view->result = $result;
		$view->respons = json_encode($this->respons, JSON_PRETTY_PRINT);
		
		return $view;
	}

	/**
	 * HTTP POST REQUEST
	 *
	 * For test :
	 * curl -F 'file=@/path/to/file.doc' -F 'fromfile=file.doc' -F 'tofile=file.pdf' -F 'fromtype=doc' -F 'totype=pdf' http://localhost:8888/rbcs/converter/service --output myfile.pdf
	 *
	 * fromtype and totype are optionals :
	 * curl -F 'file=@/path/to/file.doc' -F 'fromfile=file.doc' -F 'tofile=file.pdf' http://localhost:8888/rbcs/converter/service --output myfile.pdf
	 */
	public function serviceAction()
	{
		isset($_POST['fromfile']) ? $fromfile = basename($_POST['fromfile']) : $fromfile = null;
		isset($_POST['tofile']) ? $tofile = basename($_POST['tofile']) : $tofile = null;
		isset($_POST['fromtype']) ? $fromtype = $_POST['fromtype'] : $fromtype = null;
		isset($_POST['totype']) ? $totype = $_POST['totype'] : $totype = null;
		isset($_POST['download']) ? $download = $_POST['download'] : $download = true;
		
		/* */
		$workingdir = $this->application->getConfig('converter.workingDir');
		$workingCleanup = $this->application->getConfig('converter.working.cleanup');
		
		$fromfile = $workingdir . '/' . $fromfile;
		$fromfile = str_replace('/', '\\', $fromfile);
		
		if ( isset($_FILES['file']) ) {
			$uploaded = $_FILES['file'];
			if ( $uploaded['error'] == UPLOAD_ERR_OK ) {
				$tmpname = $uploaded["tmp_name"];
				move_uploaded_file($tmpname, $fromfile);
			}
		}
		
		if ( !$fromtype ) {
			$fi = pathinfo($fromfile);
			$fromtype = $fi['extension'];
		}
		if ( !$totype ) {
			$fi = pathinfo($tofile);
			$totype = $fi['extension'];
		}
		
		$helper = ConverterFactory::get()->getConverter($fromtype, $totype);
		$helper->workingDir($workingdir);
		$tofile = $workingdir . '/' . $tofile;
		
		$result = $helper->fromFile($fromfile)
			->toFile($tofile)
			->convert()
			->getResult();
		
		if ( $download ) {
			$this->download($tofile);
		}
		
		if($workingCleanup){
			@unlink($fromfile);
			@unlink($tofile);
		}
	}
}
