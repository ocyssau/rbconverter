<?php
namespace Rbcs\Controller;

use SplFileInfo as File;
use Rbcs\Converter\Factory as ConverterFactory;

/**
 */
class RbgateController extends \Application\Controller\Controller
{

	/**
	 *
	 * @var string
	 */
	public $workingDir;

	/**
	 *
	 * @var string
	 */
	public $wildspace;

	/**
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\Controller::preDispatch()
	 */
	public function preDispatch()
	{
		$this->workingDir = $this->application->getConfig('converter.workingDir');
		$this->wildspace = $this->application->getConfig('wildspace.path');
	}

	/**
	 */
	public function indexService()
	{
		$this->respons->feedback('You must specify method fromdatas or fromfiles');
	}
	
	/**
	 * 
	 */
	public function fromfileService()
	{
		$output = array();
		
		isset($_POST['fromfile']) ? $fromfile = basename($_POST['fromfile']) : $fromfile = null;
		isset($_POST['tofile']) ? $tofile = basename($_POST['tofile']) : $tofile = null;
		isset($_POST['fromtype']) ? $fromtype = $_POST['fromtype'] : $fromtype = null;
		isset($_POST['totype']) ? $totype = $_POST['totype'] : $totype = null;
		isset($_POST['download']) ? $download = $_POST['download'] : $download = true;
	}
	
	/**
	 */
	public function fromdatasService()
	{
		isset($_POST['datas']) ? $datas = $_POST['datas'] : $datas = array();
		$output = array();

		foreach( $datas as $data ) {
			$tmpfile = new \SplTempFileObject();
			$tmpfile->fwrite(base64_decode($data['base64data']));
			$output[] = $this->convert($tmpfile);
		}
	}

	/**
	 * Convert files defined in input
	 * Input is a array of array where key0 is the filename and key1 is the document id.
	 * The file filename must be in the wildspace directory.
	 *
	 * $input = $_GET
	 *
	 * @var $input array( array(
	 *      0=>string filename
	 *      1=>string documentId
	 *      )
	 *      )
	 *
	 */
	public function fromwsfilesService()
	{
		isset($_GET['files']) ? $files = $_GET['files'] : $files = array();
		$output = array();
		$wildspace = $this->wildspace;
		$i=0;
		foreach( $files as $file ) {
			$i++;
			if ( is_array($file) ) {
				$fileName = $file[0];
				$documentId = $file[1];
			}
			elseif ( is_string($file) ) {
				$fileName = $file;
			}
			try {
				$file = new File($wildspace . '/' . $fileName);
				$this->convert($file, $i);
			}
			catch( \Exception $e ) {
				continue;
			}
		}
	}

	/**
	 *
	 * @param File $file
	 * @throws \Exception
	 */
	protected function convert(File $file, $index)
	{
		$fromType = strtolower($file->getExtension());
		$toType = 'ranchbe';
		$workingDir = $this->workingDir;

		$converter = ConverterFactory::get()->getConverter($fromType, $toType);
		$converter->workingDir($workingDir);
		
		$fullpath = $file->getRealPath();
		if(!$fullpath){
			$this->respons->error(sprintf('file %s is not existing', $file->getFileName()));
			return;
		}
		
		$result = $converter->fromFile($fullpath)
			->convert()
			->getResult();

		$this->respons->setDatas('data'.$index, $result->getDatas());
		$this->respons->setFeedbacks($result->getFeedback());
		$errors = $result->getErrors();
		if ( $errors ) {
			$this->respons->setErrors($errors);
		}
	}
}
