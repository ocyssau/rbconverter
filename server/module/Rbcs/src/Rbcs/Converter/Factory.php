<?php
namespace Rbcs\Converter;

use Rbcs\Converter\Exception\FactoryException;

/**
 */
class Factory
{

	/**
	 *
	 * @var Factory
	 */
	protected static $instance;

	/**
	 *
	 * @var array
	 */
	protected $map = [];

	/**
	 * Singleton
	 *
	 * @return Factory
	 */
	public static function get()
	{
		if ( !self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 *
	 * @param array $map
	 *        	Is array where key is FromtypeToTotype and value the full name of class to use
	 * @return \Rbcs\Converter\Factory
	 */
	public function setMap(array $map)
	{
		$this->map = $map;
		return $this;
	}

	/**
	 *
	 * @param string $fromMtype
	 * @param string $toMtype
	 * @return ConverterInterface
	 */
	public function getConverter($fromType, $toType)
	{
		$fromType = strtolower(trim($fromType, '. '));
		$toType = strtolower(trim($toType, '. '));
		$c = ucfirst($fromType) . 'To' . ucfirst($toType);
		
		if ( isset($this->map[$c]) ) {
			$converterClass = $this->map[$c];
			
		}
		else {
			$converterClass = '\Rbcs\Converter\\' . $c;
		}
		
		if(class_exists($converterClass)){
			$converter = new $converterClass();
		}
		else{
			throw new FactoryException("$converterClass is not instanciable :", 1000);
		}
		
		return $converter;
	}
}
