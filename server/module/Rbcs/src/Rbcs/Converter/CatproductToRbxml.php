<?php
namespace Rbcs\Converter;

class CatproductToRbxml extends AbstractConverter
{

	/**
	 *
	 * @var \DOMDocument
	 */
	protected $_xmlDoc;

	/**
	 *
	 * @return boolean
	 */
	public function convert()
	{
		// Open a xml and set root node
		$this->_xmlDoc = new \DOMDocument('1.0', 'utf-8');
		$this->_xmlDoc->formatOutput = true; // Bug PHP5.2, indent is not apply
		
		$activeDocument = & $this->_openDocument($this->getInputFile());
		$activeProduct = $activeDocument->Product;
		$activeProducts = $activeProduct->Products;
		$countChilds = $activeProducts->Count();
		// echo $activeProduct->Name.'<br>';
		$activeFilePath = $activeProduct->ReferenceProduct->Parent->FullName;
		$activeDocumentName = $activeDocument->Name;
		
		// Create a root node in xml doc
		$ret['docName'] = $activeDocumentName;
		$ret['partNumber'] = $activeProduct->PartNumber;
		$ret['filePath'] = $activeFilePath;
		$ret['instanceName'] = $activeProduct->Name;
		$rootXmlElement = $this->_addXmlProduct($ret, $ret['instanceName'], $this->_xmlDoc);
		
		$this->_parseProduct($activeProduct, $activeDocumentName, 0, array(), $rootXmlElement);
		// echo $activeFilePath.'<br>';
		
		$this->_data = $this->_xmlDoc->saveXML();
		$this->_xmlDoc->save($this->getOutputFile());
		
		return true;
	}

	/**
	 * $level is used to create the product object path use by methods like _getPosition
	 * This path is use to retrieve the product object from the activeProduct
	 * $productObjectPath[$level] = current item id (1,2,3...);
	 */
	protected function _parseProduct($product, $activeDocumentName, $level = 0, $productObjectPath = array(), $xmlParentNode)
	{
		
		// $products = $product->Products;
		$itemCounter = 1; // to generate a object path to product
		$ret = array();
		
		foreach( $product->Products as $childProduct ) {
			// echo $childProduct->Name.'<br>';
			// echo $childProduct->GetMasterShapeRepresentationPathName().'<br>';
			
			$ret = array();
			
			try {
				$childDocument = $childProduct->ReferenceProduct->Parent;
				$ret['docName'] = $childDocument->Name;
				$ret['filePath'] = $childDocument->FullName;
				$ret['partNumber'] = $childProduct->PartNumber;
				$validDocument = true;
			}
			catch( \Exception $e ) {
				$ret['docName'] = '';
				$ret['filePath'] = '';
				$ret['partNumber'] = '';
				$validDocument = false;
			}
			
			if ( $childProduct->HasAMasterShapeRepresentation() ) {
				$ret['shape_filePath'] = $childProduct->GetMasterShapeRepresentationPathName();
				$pathInfos = pathinfo($ret['shape_filePath']);
				$ret['shape_docName'] = $pathInfos['basename'];
				$ret['shape_partNumber'] = $pathInfos['filename'];
			}
			
			$productObjectPath[$level] = $itemCounter;
			
			$positionMatrix = array();
			$gravityCenter = array();
			$inertiaMatrix = array();
			
			$ret['instanceName'] = $childProduct->Name;
			
			$positionMatrix = $this->_getPosition($productObjectPath);
			$ret['Ux'] = $positionMatrix[0];
			$ret['Uy'] = $positionMatrix[1];
			$ret['Uz'] = $positionMatrix[2];
			$ret['Vx'] = $positionMatrix[3];
			$ret['Vy'] = $positionMatrix[4];
			$ret['Vz'] = $positionMatrix[5];
			$ret['Wx'] = $positionMatrix[6];
			$ret['Wy'] = $positionMatrix[7];
			$ret['Wz'] = $positionMatrix[8];
			$ret['Tx'] = $positionMatrix[9];
			$ret['Ty'] = $positionMatrix[10];
			$ret['Tz'] = $positionMatrix[11];
			
			if ( $validDocument ) {
				$gravityCenter = $this->_getGravityCenter($productObjectPath);
				$ret['Cgx'] = $gravityCenter[0];
				$ret['Cgy'] = $gravityCenter[1];
				$ret['Cgz'] = $gravityCenter[2];
				
				$inertiaMatrix = $this->_getInertia($productObjectPath);
				$ret['Ixx'] = $inertiaMatrix[0];
				$ret['Ixy'] = $inertiaMatrix[1];
				$ret['Ixz'] = $inertiaMatrix[2];
				$ret['Iyx'] = $inertiaMatrix[3];
				$ret['Iyy'] = $inertiaMatrix[4];
				$ret['Iyz'] = $inertiaMatrix[5];
				$ret['Izx'] = $inertiaMatrix[6];
				$ret['Izy'] = $inertiaMatrix[7];
				$ret['Izz'] = $inertiaMatrix[8];
				// var_dump($inertiaMatrix);
			}
			else {
				$ret['Cgx'] = '';
				$ret['Cgy'] = '';
				$ret['Cgz'] = '';
				$ret['Ixx'] = '';
				$ret['Ixy'] = '';
				$ret['Ixz'] = '';
				$ret['Iyx'] = '';
				$ret['Iyy'] = '';
				$ret['Iyz'] = '';
				$ret['Izx'] = '';
				$ret['Izy'] = '';
				$ret['Izz'] = '';
			}
			
			$xmlElement = & $this->_addXmlProduct($ret, $ret['instanceName'], $xmlParentNode);
			
			if ( $ret['docName'] == $activeDocumentName ) { // Then its a component
				$this->_parseProduct($childProduct, $activeDocumentName, $level + 1, $productObjectPath, $xmlElement);
			}
			
			$itemCounter++;
		} // end loop foreach
		
		return $ret;
	}

	/**
	 */
	protected function _addXmlProduct(array $productProperties, $id, &$xmlParentNode)
	{
		$productElement = $this->_xmlDoc->createElement($id, $id);
		$xmlParentNode->appendChild($productElement);
		foreach( $productProperties as $productPropertyName => $productPropertyValue ) {
			$element = $this->_xmlDoc->createElement($productPropertyName, $productPropertyValue);
			$productElement->appendChild($element);
		}
		return $productElement;
	}
} /* End of class */
