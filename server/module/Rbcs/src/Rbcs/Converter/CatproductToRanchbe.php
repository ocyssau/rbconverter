<?php
namespace Rbcs\Converter;

use Rbcs\Catia;

class CatproductToRanchbe extends AbstractConverter
{

	/**
	 *
	 * @param
	 *        	array
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->catia = new Catia\Application();
		$this->catia->connect();
		
		$this->fromType = 'catproduct';
		$this->toType = 'ranchbe';
		$this->result->setData('fromType', 'catproduct');
		$this->result->setData('toType', 'ranchbe');
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see Rbcs\Converter.ConverterInterface::convert()
	 *
	 * @return CatproductToRanchbe
	 */
	public function convert()
	{
		// return $this->convertToStep();
		$workingDir = $this->workingDir;
		
		$catiaDocument = & $this->catia->readDocument($this->fromFile)->activeDocument;
		$this->catiaDocument = $catiaDocument;
		
		$id = uniqId();
		if(!$this->toFile){
			$jsonFile = $workingDir . '/' . $id . '.json';
		}
		else{
			$jsonFile = $workingDir . '/' . basename($this->toFile);
		}
		
		$errFile = $workingDir . '/' . $id . '.err';
		$stepFile = $workingDir . '/' . $id . '.stp';
		
		$this->result->setData('files', [
			$jsonFile,
			$errFile,
			$stepFile
		]);
		
		try {
			$product = new \Rbcs\Catia\ProductDocument($catiaDocument, $this->catia);
			$this->result->setData('properties', $product->getArrayCopy());
			
			$children = $product->getChildren();
			$links = $product->getProductInstanceLinks();
			
			for ($i = 0; $i < count($children); $i++) {
				$link = $links[$i];
				$pathParts = pathinfo($link['lnk']);
				$children[$i]['ofDocumentName'] = $pathParts['basename'];
				$children[$i]['ofProductName'] = $pathParts['filename'];
				$children[$i]['ofProductNumber'] = $pathParts['filename'];
			}
			
			$this->result->setData('children', $children);
			$this->result->setData('links', $product->getProductInstanceLinks());
		}
		catch( \Rbcs\Catia\ProductWalker\Exception $e ) {
			$errors = explode(';', $e->getMessage());
			$this->result->error($errors);
			if ( $e->children ) {
				$this->result->setData('children', $e->children);
			}
		}
		catch( \Exception $e ) {
			$this->result->error($e->getMessage());
		}
		
		/* Save to step file */
		/*
		try {
			if ( is_file($stepFile) ) {
				unlink($stepFile);
			}
			
			$product = new \Rbcs\Catia\ProductDocument($catiaDocument, $this->catia);
			$product->toStep($stepFile);
			$this->result->setData('stepfile', $stepFile);
			$this->result->setData('stepdatas', file_get_contents($stepFile));
			
			$this->result->setData('step', array(
					'file' => $stepFile,
					'type' => 'step'
			));
		}
		catch( \Exception $e ) {
			$this->result->error($e->getMessage());
		}
		*/
		
		//var_dump($this->result->toJson());
		
		/* Save to json file */
		try {
			$this->result->setData('json', array(
				'file' => $jsonFile,
				'type' => 'json'
			));
			file_put_contents($jsonFile, $this->result->toJson());
			file_put_contents($errFile, print_r($this->result->getErrors(), true));
			chmod($jsonFile, 0777);
			chmod($errFile, 0777);
		}
		catch( \Exception $e ) {
			$this->result->error($e->getMessage());
		}
		
		$this->catia->closeDocument($catiaDocument);
		return $this;
	}
} /* End of class */
