<?php
namespace Rbcs\Converter;

class CatdrawingToPs extends AbstractConverter
{

	public function convert()
	{
		$document = $this->openDocument($this->getInputFile());
		$document->ExportData($this->getOutputFile(), 'ps');
	}
} /* End of class */
