<?php
namespace Rbcs\Converter;

class PptToPdf extends Ooo
{

	/**
	 *
	 * @param
	 *        	array
	 */
	public function __construct()
	{
		parent::__construct();
		$this->fromType = 'ppt';
		$this->toType = 'pdf';
		$this->result->setData('fromType', $this->fromType);
		$this->result->setData('toType', $this->toType);
	}
} /* End of class */

