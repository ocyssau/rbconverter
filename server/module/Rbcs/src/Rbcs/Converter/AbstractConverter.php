<?php
namespace Rbcs\Converter;

abstract class AbstractConverter implements ConverterInterface
{

	/**
	 * Full path to file to convert with extension and file name
	 *
	 * @var string
	 */
	protected $fromFile;

	/**
	 * Full path to result file with extension and file name
	 *
	 * @var string
	 */
	protected $toFile;

	/**
	 * Full path to result directory
	 *
	 * @var string
	 */
	protected $workingDir;
	
	/**
	 * Data to convert
	 */
	protected $data;

	/**
	 * Resulted datas
	 *
	 * @var Result
	 */
	protected $result;

	/**
	 * Type of original file: odt,doc...
	 *
	 * @var string
	 */
	protected $fromType;

	/**
	 * Type of result file
	 *
	 * @var string
	 */
	protected $toType;

	/**
	 *
	 * @var array
	 */
	protected $formats = array();

	/**
	 *
	 * @param
	 *        	array
	 */
	public function __construct()
	{
		$this->result = new Result();
	}

	/**
	 *
	 * @param string $file        	
	 * @return ConverterInterface
	 */
	public function fromFile($file)
	{
		$this->fromFile = $file;
		return $this;
	}

	/**
	 * Set full path to output file
	 * 
	 * @param string $file        	
	 * @return ConverterInterface
	 */
	public function toFile($file)
	{
		$this->toFile = $file;
		return $this;
	}

	/**
	 * Set full path to working directory where put result
	 *
	 * @param string $path        	
	 * @return ConverterInterface
	 */
	public function workingDir($path)
	{
		$path = realpath($path);
		if ( is_null($path) || !is_dir($path) ) {
			throw new \Exception("$path is not a directory");
		}
		$this->workingDir = $path;
		return $this;
	}
	
	/**
	 *
	 * @param string $type        	
	 * @return ConverterInterface
	 */
	public function fromType($type)
	{
		$this->fromType = $type;
		return $this;
	}

	/**
	 *
	 * @param string $type        	
	 * @return ConverterInterface
	 */
	public function toType($type)
	{
		$this->toType = $type;
		return $this;
	}

	/**
	 *
	 * @return ConverterInterface
	 */
	public abstract function convert();

	/**
	 *
	 * @return Result
	 */
	public function getResult()
	{
		return $this->result;
	}
}