<?php
namespace Rbcs\Converter;

class CatdrawingToPdf extends AbstractConverter
{

	/**
	 */
	public function convert()
	{
		$document = $this->openDocument($this->fromFile);
		$document->ExportData($this->getOutputFile(), 'pdf');
	}
} /* End of class */
