<?php
namespace Rbcs\Converter;

use Rbcs\Catia;

class CatdrawingToRanchbe extends AbstractConverter
{

	/**
	 *
	 * @param
	 *        	array
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->catia = new Catia\Application();
		$this->catia->connect();
		
		$this->fromType = 'catdrawing';
		$this->toType = 'ranchbe';
		$this->result->setData('fromType', 'catdrawing');
		$this->result->setData('toType', 'ranchbe');
	}

	/**
	 */
	public function convert()
	{
		$workingDir = $this->workingDir;
		$catiaDocument = & $this->catia->openDocument($this->fromFile)->activeDocument;
		
		$this->catiaDocument = $catiaDocument;
		$id = uniqid();
		$pdfFile = $workingDir . '/' . $id . '.pdf';
		$drawing = new \Rbcs\Catia\DrawingDocument($catiaDocument, $this->catia);
		
		/* Extract properties */
		try {
			$drawing->loadSheets();
			$this->result->setData('properties', $drawing->getArrayCopy());
		}
		catch( \Exception $e ) {
			$this->result->error($e->getMessage());
		}
		
		/* Capture pdf */
		try {
			$drawing->toPdf($pdfFile);
			$this->result->setData('picture', array(
				'file' => $pdfFile,
				'type' => 'pdf'
			));
		}
		catch( \Exception $e ) {
			$this->result->error($e->getMessage());
		}
		
		$this->catia->closeDocument($catiaDocument);
		
		return $this;
	}
} /* End of class */
