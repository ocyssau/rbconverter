<?php 

namespace Rbcs\Converter\Exception;


/**
 * Exception for factory
 * @author o_cyssau
 *
 */
class FactoryException extends \Exception
{
}

