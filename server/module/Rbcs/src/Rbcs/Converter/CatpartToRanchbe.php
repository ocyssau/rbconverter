<?php
namespace Rbcs\Converter;

use Rbcs\Catia;

class CatpartToRanchbe extends AbstractConverter
{

	/**
	 *
	 * @param
	 *        	array
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->catia = new Catia\Application();
		$this->catia->connect();
		
		$this->fromType = 'catpart';
		$this->toType = 'ranchbe';
		$this->result->setData('fromType', 'catpart');
		$this->result->setData('toType', 'ranchbe');
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see Rbcs\Converter.ConverterInterface::convert()
	 */
	public function convert()
	{
		$workingDir = $this->workingDir;
		$catiaDocument = & $this->catia->openDocument($this->fromFile)->activeDocument;
		$this->catiaDocument = $catiaDocument;
		
		$id = uniqId();
		if(!$this->toFile){
			$jsonFile = $workingDir . '/' . $id . '.json';
		}
		else{
			$jsonFile = $workingDir . '/' . basename($this->toFile);
		}
		
		$errFile = $workingDir . '/' . $id . '.err';
		$stepFile = $workingDir . '/' . $id . '.stp';
		$jpgFile = $workingDir . '/' . $id . '.jpg';
		$wrlFile = $workingDir . '/' . $id . '.wrl';
		$igsFile = $workingDir . '/' . $id . '.igs';
		$stlFile = $workingDir . '/' . $id . '.stl';
		
		$this->result->setData('files', [
			$jsonFile,
			$errFile,
			$stepFile,
			$jpgFile,
			$wrlFile,
			$igsFile,
			$stlFile
		]);
		
		/* Extract properties */
		try {
			$part = new \Rbcs\Catia\PartDocument($catiaDocument, $this->catia);
			try {
				$part->loadPhysicalProperties();
			}
			catch( \Rbcs\Catia\Exception\PhysicalPropertyException $e ) {
				$this->result->feedback($e->getMessage());
			}
			try {
				$part->loadMaterials();
			}
			catch( \Rbcs\Catia\Exception\PhysicalPropertyException $e ) {
				$this->result->feedback($e->getMessage());
			}
			try {
				$part->loadGravityCenter();
			}
			catch( \Rbcs\Catia\Exception\PhysicalPropertyException $e ) {
				$this->result->feedback($e->getMessage());
			}
		}
		catch( \Exception $e ) {
			$this->result->error($e->getMessage());
		}
		
		/* Extract properties */
		$this->result->setData('properties', $part->getArrayCopy());
		
		/* Capture picture */
		try {
			$part->toJpg($jpgFile);
			$this->result->setData('picture', array(
				'file' => $jpgFile,
				'type' => 'jpg',
				'data' => base64_encode(file_get_contents($jpgFile))
			));
			chmod($jpgFile, 0777);
		}
		catch( \Exception $e ) {
			$this->result->error($e->getMessage());
		}
		
		/* Convert 3d Igs */
		/*
		try {
			$part->toIgs($igsFile);
			$this->result->setData('convert', array(
				'file' => $igsFile,
				'type' => 'igs'
			));
			chmod($igsFile, 0777);
		}
		catch( \Exception $e ) {
			$this->result->error($e->getMessage());
		}
		*/
		
		/* Convert 3d STL */
		try {
			$part->toStl($stlFile);
			$this->result->setData('3d', array(
				'file' => $stlFile,
				'type' => 'stl',
				'data' => base64_encode(file_get_contents($stlFile))
			));
			chmod($stlFile, 0777);
		}
		catch( \Exception $e ) {
			$this->result->error($e->getMessage());
		}
		
		/* Save to json file */
		try {
			$this->result->setData('json', array(
				'file' => $jsonFile,
				'type' => 'json',
			));
			file_put_contents($jsonFile, $this->result->toJson());
			file_put_contents($errFile, print_r($this->result->getErrors(), true));
			chmod($jsonFile, 0777);
			chmod($errFile, 0777);
		}
		catch( \Exception $e ) {
			$this->result->error($e->getMessage());
		}
		
		$this->catia->closeDocument($catiaDocument);
		return $this;
	}
} /* End of class */
