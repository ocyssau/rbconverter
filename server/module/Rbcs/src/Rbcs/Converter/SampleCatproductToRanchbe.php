<?php
namespace Rbcs\Converter;

/**
 */
class SampleCatproductToRanchbe implements ConverterInterface
{

	/**
	 * Data to convert
	 */
	protected $data;

	/**
	 * Resulted datas
	 *
	 * @var Result
	 */
	protected $result;

	/**
	 *
	 * @var string
	 */
	protected $jsonSampleFile;

	/**
	 *
	 * @param
	 *        	array
	 */
	public function __construct()
	{
		$this->result = new Result();
		$this->jsonSampleFile = 'data/samples/exportCatproduct_exemple.txt';
	}

	/**
	 * Set full path to working directory where put result
	 *
	 * @param string $path
	 * @return ConverterInterface
	 */
	public function workingDir($path)
	{
		$path = realpath($path);
		if ( is_null($path) || !is_dir($path) ) {
			throw new \Exception("$path is not a directory");
		}
		$this->workingDir = $path;
		return $this;
	}

	/**
	 *
	 * @param string $file
	 * @return ConverterInterface
	 */
	public function fromFile($file)
	{
		return $this;
	}

	/**
	 * Set full path to output file
	 *
	 * @param string $file
	 * @return ConverterInterface
	 */
	public function toFile($file)
	{
		$this->toFile = $file;
		return $this;
	}

	/**
	 *
	 * @param string $type
	 * @return ConverterInterface
	 */
	public function fromType($type)
	{
		return $this;
	}

	/**
	 *
	 * @param string $type
	 * @return ConverterInterface
	 */
	public function toType($type)
	{
		return $this;
	}
	
	/**
	 *
	 * @return ConverterInterface
	 */
	public function convert()
	{
		$json = file_get_contents($this->jsonSampleFile);
		
		$workingDir = $this->workingDir;
		$id = uniqId();
		if ( !$this->toFile ) {
			$jsonFile = $workingDir . '/' . $id . '.json';
		}
		else {
			$jsonFile = $workingDir . '/' . basename($this->toFile);
		}
		
		/* Save to json file */
		try {
			file_put_contents($jsonFile, $json);
			chmod($jsonFile, 0777);
		}
		catch( \Exception $e ) {
			$this->result->error($e->getMessage());
		}
		
		$this->result->setData('files', []);
		
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getResult()
	{
		return $this->result;
	}
} /* End of class */
