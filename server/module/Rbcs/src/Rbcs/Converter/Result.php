<?php
namespace Rbcs\Converter;

/**
 * 
 * @author o_cyssau
 *
 */
class Result
{

	/**
	 * 
	 * @var array
	 */
	protected $datas;

	/**
	 * 
	 * @var array
	 */
	protected $feedbacks;

	/**
	 * 
	 * @var array
	 */
	protected $errors;

	/**
	 *
	 * @return string
	 */
	public function toJson()
	{
		return json_encode($this->datas, JSON_FORCE_OBJECT | JSON_PRETTY_PRINT);
	}
	
	/**
	 *
	 * @param string $name        	
	 * @param mixed $datas
	 * @return Result        	
	 */
	public function setData($name, $datas)
	{
		$this->datas[$name] = $datas;
		return $this;
	}

	/**
	 *
	 * @param string $msg    
	 * @return Result        	
	 */
	public function error($msg)
	{
		if(is_array($msg)){
			foreach($msg as $m){
				$this->errors[] = (string)$m;
			}
		}
		else{
			$this->errors[] = (string)$msg;
		}
		return $this;
	}

	/**
	 *
	 * @param string $msg
	 * @return Result        	
	 */
	public function feedback($msg)
	{
		if(is_array($msg)){
			foreach($msg as $m){
				$this->feedbacks[] = (string)$m;
			}
		}
		$this->feedbacks[] = (string)$msg;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getDatas()
	{
		return $this->datas;
	}

	/**
	 *
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 *
	 * @return array
	 */
	public function getFeedback()
	{
		return $this->feedbacks;
	}
}
