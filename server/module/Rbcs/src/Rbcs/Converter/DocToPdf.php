<?php
namespace Rbcs\Converter;

class DocToPdf extends Ooo
{

	/**
	 *
	 * @param
	 *        	array
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->fromType = 'doc';
		$this->toType = 'pdf';
		$this->result->setData('fromType', $this->fromType);
		$this->result->setData('toType', $this->toType);
	}
	
} /* End of class */
