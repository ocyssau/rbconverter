<?php
namespace Rbcs\Converter;

use Rbcs\Catia\CatCaptureFormat;

/**
 * 
 *
 */
class CatpartToJpg extends AbstractConverter
{

	/**
	 *
	 * @param
	 *        	array
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->formats = array(
			'cgm' => CatCaptureFormat::catCaptureFormatCGM,
			'emf' => CatCaptureFormat::catCaptureFormatEMF,
			'tif' => CatCaptureFormat::catCaptureFormatTIFF,
			'tiff' => CatCaptureFormat::catCaptureFormatTIFF,
			'tifg' => CatCaptureFormat::catCaptureFormatTIFFGreyScale,
			'gtif' => CatCaptureFormat::catCaptureFormatTIFFGreyScale,
			'bmp' => CatCaptureFormat::catCaptureFormatBMP,
			'jpg' => CatCaptureFormat::catCaptureFormatJPEG
		);
	}
	
	/**
	 */
	public function convert()
	{
		$workingDir = $this->workingDir;
		$catiaDocument = & $this->catia->openDocument($this->fromFile)->activeDocument;
		
		return $this;
	}
	
	
}
