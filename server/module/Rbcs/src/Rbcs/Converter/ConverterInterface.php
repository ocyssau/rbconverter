<?php
namespace Rbcs\Converter;

interface ConverterInterface
{

	/**
	 *
	 * @param string $file        	
	 * @return ConverterInterface
	 */
	public function fromFile($file);

	/**
	 *
	 * @param string $file        	
	 * @return ConverterInterface
	 */
	public function toFile($file);

	/**
	 *
	 * @param string $type        	
	 * @return ConverterInterface
	 */
	public function fromType($type);

	/**
	 *
	 * @param string $type        	
	 * @return ConverterInterface
	 */
	public function toType($type);

	/**
	 *
	 * @return ConverterInterface
	 */
	public function convert();

	/**
	 *
	 * @return string
	 */
	public function getResult();
}

