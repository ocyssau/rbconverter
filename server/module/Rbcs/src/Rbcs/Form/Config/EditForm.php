<?php
namespace Rbcs\Form\Config;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class EditForm extends Form implements InputFilterProviderInterface
{

	public $template;

	public $rbcs;

	protected $inputFilter;

	/**
	 *
	 * @param string $name        	
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('configEdit');
		
		$this->template = 'rbcs/config/editform';
		$this->rbcs = \Rbcs::get();
		
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());
		
		$inputFilter = $this->getInputFilter();
		
		/* NOTE: . in name must eb replaced by _ */
		
		/* converter.workingDir */
		$this->add(array(
			'name' => 'converter_workingDir',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'path to directory',
				'class' => 'form-control fileDialog',
				'size' => 60
			),
			'options' => array(
				'label' => 'Working Directory'
			)
		));
		$inputFilter->add(array(
			'name' => 'converter_workingDir',
			'required' => true,
			'validators' => array()
		));
		
		/* wildspace.path */
		$this->add(array(
			'name' => 'wildspace_path',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'path to directory',
				'class' => 'form-control fileDialog',
				'size' => 60
			),
			'options' => array(
				'label' => 'Wildspace path'
			)
		));
		$inputFilter->add(array(
			'name' => 'wildspace_path',
			'required' => true,
			'validators' => array()
		));
		
		/* converter.working.cleanup */
		$this->add(array(
			'name' => 'converter_working_cleanup',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control',
			),
			'options' => array(
				'use_hidden_element' => true,
				'checked_value' => 1,
				'unchecked_value' => 0,
				'label' => 'Delete file after work'
			)
		));
		$inputFilter->add(array(
			'name' => 'converter_working_cleanup',
			'required' => false,
			'validators' => array()
		));
		
		
		/* catia.version */
		$mask = '/v[0-9]{1}r[0-9]{2}/i';
		$this->add(array(
			'name' => 'catia_version',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'v0r00',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Version Of Catia'
			)
		));
		$inputFilter->add(array(
			'name' => 'catia_version',
			'required' => false,
			'validators' => array(
				array(
					'name' => 'Regex',
					'options' => array(
						'pattern' => $mask,
						'messages' => array(
							\Zend\Validator\Regex::INVALID => 'Must be formated as v[number]r[number]. Example: v5r21'
						)
					)
				)
			)
		));
		
		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));
		
		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Cancel',
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array();
	}
}
