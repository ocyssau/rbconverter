<?php
namespace Rbcs\Form\Rbgate;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class ConverterForm extends Form implements InputFilterProviderInterface
{

	/**
	 *
	 * @var
	 *
	 */
	public $rbcs;

	/**
	 *
	 * @param string $name        	
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('configEdit');
		
		$this->rbcs = \Rbcs::get();
		
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());
		
		/* fromFile */
		$this->add(array(
			'name' => 'upload',
			'type' => 'Zend\Form\Element\File',
			'attributes' => array(
				'placeholder' => 'Select file to convert',
				'class' => 'form-file'
			),
			'options' => array(
				'label' => 'File to convert'
			)
		));
		
		/* toMtype */
		$this->add(array(
			'name' => 'toType',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'To mtype',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Convert to type',
				'options' => $this->_getTypes()
			)
		));
		
		/* fromMtype */
		$this->add(array(
			'name' => 'fromType',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'From type',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Convert from type',
				'options' => $this->_getTypes()
			)
		));
		
		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));
		
		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Cancel',
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'upload' => array(
				'required' => true
			),
			'toType' => array(
				'required' => true,
				'validators' => []
			),
			'fromType' => array(
				'required' => true,
				'validators' => []
			)
		);
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	protected function _getTypes()
	{
		return array(
			'null' => 'Automatic',
			'catdrawing' => 'Catdrawing',
			'catpart' => 'Catpart',
			'catproduct' => 'Catproduct',
			'ranchbe' => 'Ranchbe',
			'doc' => 'Doc',
			'ooo' => 'Ooo',
			'ppt' => 'Ppt',
			'pdf' => 'pdf',
			'xls' => 'xls',
			'ps' => 'ps',
			'jpg' => 'jpg'
		);
	}
}
