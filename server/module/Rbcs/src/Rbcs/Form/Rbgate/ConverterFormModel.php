<?php
namespace Rbcs\Form\Rbgate;

use Exception;

/**
 * Rbcs\Converter is the main class of the soap server.
 * This class is used to defined
 * the wsdl file.
 * Only public method are published by th soap server
 */
class ConverterFormModel
{

	/**
	 * Uploaded file
	 *
	 * @var array( 'name',
	 *      'type',
	 *      'size',
	 *      'tmp_name'
	 *      'error'
	 *      )
	 */
	protected $uploadedFile;

	/**
	 * From file fullpath
	 *
	 * @var string
	 */
	protected $fromFile;

	/**
	 * Full path to result file with extension and file name
	 *
	 * @var string
	 */
	protected $toFile;

	/**
	 * Base 64 encoded data to convert
	 *
	 * @var string base64
	 */
	protected $data;

	/**
	 * Type of original file: odt,doc...
	 *
	 * @var string
	 */
	protected $fromType;

	/**
	 * Type of result file
	 *
	 * @var string
	 */
	protected $toType;

	/**
	 * Directory where is original file and converted file
	 *
	 * @var string
	 */
	protected $workingDir;

	/**
	 * Array of transports used
	 *
	 * @var array
	 */
	protected $transport = array(
		'Ftp' => false,
		'Directory' => false,
		'Soap' => false
	);

	/**
	 *
	 * @param array $config
	 */
	public function __construct($path)
	{
		$path = realpath($path);
		if ( is_null($path) || !is_dir($path) ) {
			throw new Exception("$path is not a directory");
		}
		
		$this->workingDir = $path;
	}

	/**
	 *
	 * @param array $properties
	 */
	public function populate($properties)
	{
		/* set fromType before setFile */
		isset($properties['fromType']) ? $this->fromType = $properties['fromType'] : null;
		isset($properties['toType']) ? $this->toType = $properties['toType'] : null;

		if ( isset($properties['upload']) ) {
			$uploaded = $properties['upload'];
			if ( $uploaded['error'] == 0 ) {
				$fromfile = $this->workingDir . '/' . $uploaded['name'];
				copy($uploaded['tmp_name'], $fromfile);
				$this->setFromfile($fromfile);
			}
		}
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		return array(
			'fromFile' => $this->fromFile,
			'fromType' => $this->fromType,
			'toType' => $this->toType
		);
	}

	/**
	 *
	 * @param string $filepath
	 */
	public function setFromfile($filepath)
	{
		if ( is_file($filepath) ) {
			$this->fromFile = realpath($filepath);
			$this->fromType = substr($this->fromFile, strrpos($this->fromFile, '.')+1);
		}
		else {
			new Exception($filepath . ' is not a file');
		}
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getFromfile()
	{
		return $this->fromFile;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getTofile()
	{
		if(!$this->toFile){
			$finfo = pathinfo($this->fromFile);
			$this->toFile = $finfo['filename'].'.'.$this->toType;
		}
		return $this->toFile;
	}

	/**
	 *
	 * @param string $type
	 * @return ConverterFormModel
	 */
	public function setFromtype($type)
	{
		$this->fromType = strtolower($type);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getFromtype()
	{
		return $this->fromType;
	}

	/**
	 *
	 * @param string $type
	 * @return ConverterFormModel
	 */
	public function setTotype($type)
	{
		$this->toType = strtolower($type);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getTotype()
	{
		return $this->toType;
	}
} /* End of class */
