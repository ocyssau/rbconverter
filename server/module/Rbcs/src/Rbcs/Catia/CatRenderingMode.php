<?php
namespace Rbcs\Catia;

class CatRenderingMode
{

	const catRenderShading = 0;

	const catRenderShadingWithEdges = 1;

	const catRenderWireFrame = 2;

	const catRenderHiddenLinesRemoval = 3;

	const catRenderQuickHiddenLinesRemoval = 4;

	const catRenderMaterial = 5;

	const catRenderMaterialWithEdges = 6;

	const catRenderShadingWithEdgesAndHiddenEdges = 7;

	const catRenderShadingWithEdgesWithoutSmoothEdges = 8;
}
