<?php
namespace Rbcs\Catia;

class CatSpecsAndGeomWindowLayout
{

	const catWindowSpecsOnly = 0;

	const catWindowGeomOnly = 1;

	const catWindowSpecsAndGeom = 2;
}
