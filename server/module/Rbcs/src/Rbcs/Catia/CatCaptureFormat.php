<?php
namespace Rbcs\Catia;

class CatCaptureFormat
{

	const catCaptureFormatCGM = 0;

	const catCaptureFormatEMF = 1;

	const catCaptureFormatTIFF = 2;

	const catCaptureFormatTIFFGreyScale = 3;

	const catCaptureFormatBMP = 4;

	const catCaptureFormatJPEG = 5;
}
