<?php
namespace Rbcs\Catia;

/**
 * 
 *
 */
class PartDocument
{

	/**
	 *
	 * @var \stdClass
	 */
	public $document;

	/**
	 *
	 * @var \stdClass
	 */
	public $product;

	/**
	 * 
	 * @var string
	 */
	public $fullName;

	/**
	 * 
	 * @var string
	 */
	public $name;

	/**
	 * 
	 * @var string
	 */
	public $path;

	/**
	 * 
	 * @var boolean
	 */
	public $readonly;

	/**
	 * 
	 * @var boolean
	 */
	public $saved;

	/**
	 *
	 * @var array
	 */
	protected $shape;

	/**
	 *
	 * @var array
	 */
	protected $physicalProperties;

	/**
	 *
	 * @var array
	 */
	protected $materials;

	/**
	 *
	 * @var array
	 */
	protected $gravityCenter;

	/**
	 *
	 * @var array
	 */
	protected $inertiaMatrix;

	/**
	 *
	 * @param \variant $catDocument        	
	 */
	public function __construct($catDocument, $catia)
	{
		$this->document = $catDocument;
		$this->catia = $catia;
		$this->product = $catDocument->product;
		$this->fullName = $catDocument->FullName;
		$this->name = $catDocument->Name;
		$this->path = $catDocument->Path;
		$this->readOnly = $catDocument->ReadOnly;
		$this->saved = $catDocument->Saved;
	}

	/**
	 *
	 * @return PartDocument
	 */
	public function loadMaterials()
	{
		$part = $this->document->Part;
		$materials = array();
		$parameters = $part->Parameters;
		
		for ($i = 1; $i <= $part->Bodies->Count; $i++) {
			/* protection against long time */
			if ( $i > 10 ) {
				break;
			}
			
			try {
				$body = $part->Bodies->item($i);
				$strParam = $parameters->item($part->Name . '\\' . $body->Name . '\Material');
				$materialName = $strParam->Value;
				$materials[] = array(
					'name' => $materialName,
					'density' => null
				);
			}
			catch( \Exception $e ) {}
		}
		$this->materials = $materials;
		return $this;
		
		/*
		 * VBA Version Sub CATMain() Dim Part, Parameters, Body Dim Bi As Integer Dim StrParam, MaterialName As String Set Part = CATIA.ActiveDocument.Part Set Parameters = Part.Parameters For Bi = 1 To Part.Bodies.Count Set Body = Part.Bodies.Item(Bi) Set StrParam = Parameters.Item(Part.Name & "\" & Body.Name & "\Material") MaterialName = StrParam.Value Next End Sub
		 */
	}

	/**
	 *
	 * @return array
	 */
	public function getWeight()
	{
		if ( !isset($this->weight) ) {
			$this->loadPhysicalProperties();
		}
		return $this->weight;
	}

	/**
	 *
	 * @return array
	 */
	public function getWetArea()
	{
		if ( !isset($this->wetArea) ) {
			$this->loadPhysicalProperties();
		}
		return $this->wetArea;
	}

	/**
	 *
	 * @return array
	 */
	public function getVolume()
	{
		if ( !isset($this->volume) ) {
			$this->loadPhysicalProperties();
		}
		return $this->volume;
	}

	/**
	 *
	 * @return array
	 */
	public function loadPhysicalProperties()
	{
		$catDocument = $this->document;
		$product = $catDocument->product;
		$part = $catDocument->part;
		
		$inertia = $product->GetTechnologicalObject('Inertia');
		$analyze = $product->Analyze;
		
		try {
			$this->volume = ($analyze->Volume); /* Volume mm3 */
			$this->wetArea = ($analyze->WetArea); /* Surface M2 */
			$this->density = ($inertia->Density); /* Density Kg/m3 */
			$this->weight = ($inertia->Mass * 1000); /* Mass in grammes */
		}
		catch( \Exception $e ) {
			throw new Exception\PhysicalPropertyException($e->getMessage());
		}
		
		return $this;
	}

	/**
	 */
	public function getArrayCopy()
	{
		$part = $this->document->part;
		
		$output = array(
			'fullName' => $this->fullName,
			'name' => $this->name,
			'number' => $this->name,
			'path' => $this->path,
			'readonly' => $this->readOnly,
			'saved' => $this->saved,
			'product' => array(
				'name' => $this->product->Name,
				'number' => $this->product->PartNumber,
				'nomenclature' => $this->product->Nomenclature,
				'version' => $this->product->Revision,
				'definition' => $this->product->Definition,
				'description' => $this->product->DescriptionRef
			),
			'physicalProperties' => array(
				'weight' => $this->weight, /*Mass in grammes*/
				'density' => $this->density, /*Density Kg/m3*/
				'wetArea' => $this->wetArea, /*Surface M2*/
				'volume' => $this->volume, /*Volume mm3*/
			),
			'gravityCenter' => $this->gravityCenter,
			'inertiaMatrix' => $this->inertiaMatrix,
			'materials' => $this->materials,
			'shape' => $this->shape
		);
		return $output;
	}

	/**
	 * Get the gravity center matrix of the product item in the product context
	 * or in the local datum
	 * to get the product in the activeProduct
	 * If $itemId is array, construct path to object like this :
	 * Set Product = ActiveProduct.Products.item(1).Products.item(1) ...etc for each value in itemPath
	 *
	 * @param \stdClass $itemId
	 *        	is id to use with method VBA CATIA Automation Products.item()
	 */
	public function loadGravityCenter()
	{
		if ( isset($this->gravityCenter) ) {
			return $this->gravityCenter;
		}
		
		$script = "
		Public Function getGravityCenter()
			Dim document As document
			Dim product As product
			Dim Inertia As Variant
			Dim cgCoordinates(2) As Variant
			Set document = CATIA.ActiveDocument
			Set product = CATIA.ActiveDocument.product
			Set Inertia = product.GetTechnologicalObject(\"Inertia\")
			Inertia.GetCOGPosition cgCoordinates
			getGravityCenter = cgCoordinates
		End Function";
		
		try {
			$scriptLanguage = 0; // CATVBScriptLanguage
			$funcName = 'getGravityCenter';
			$parameters = array();
			$catia = $this->document->Application;
			$res = $catia->SystemService->Evaluate($script, $scriptLanguage, $funcName, $parameters);
		}
		catch( \Exception $e ) {
			throw new Exception\PhysicalPropertyException($e->getMessage());
		}
		
		/* Convert variant to array */
		$keys = array(
			'x',
			'y',
			'z'
		);
		$gravityCenter = array();
		foreach( $res as $k => $v ) {
			$gravityCenter[$keys[$k]] = $v;
		}
		
		$this->gravityCenter = $gravityCenter;
		return $this->gravityCenter;
	}

	/**
	 *
	 * @param string $itemPath        	
	 */
	public function loadInertiaCenter($itemPath)
	{
		if ( isset($this->inertiaCenter) ) {
			return $this->inertiaCenter;
		}
		
		$script = "
		Public Function getInertiaMatrix()
			Dim document As document
			Dim product As product
			Dim Inertia As Variant
			Dim inertiaMatrix(8) As Variant
			Dim analyze
			Set document = CATIA.ActiveDocument
			Set product = CATIA.ActiveDocument.product
			Set analyze = product.Analyze()
			analyze.GetInertia inertiaMatrix
			getInertiaMatrix = inertiaMatrix
		End Function";
		
		try {
			$scriptLanguage = 0; // CATVBScriptLanguage
			$funcName = 'getInertiaMatrix';
			$parameters = array();
			$catia = $this->document->Application;
			$res = $catia->SystemService->Evaluate($script, $scriptLanguage, $funcName, $parameters);
		}
		catch( \Exception $e ) {
			throw new Exception\PhysicalPropertyException($e->getMessage());
		}
		
		/* Convert variant to array */
		$keys = array(
			'Ixx',
			'Ixy',
			'Ixz',
			'Iyx',
			'Iyy',
			'Iyz',
			'Izx',
			'Izy',
			'Izz'
		);
		$inertia = array();
		foreach( $res as $k => $v ) {
			$inertia[$keys[$k]] = $v;
		}
		
		$this->inertiaMatrix = $inertia;
		return $inertia;
	}

	/**
	 */
	public function loadShape($activate = true)
	{
		if ( !isset($this->shape) ) {
			$this->shape = array(
				'filePath' => null,
				'name' => null,
				'partNumber' => null
			);
			$product = $this->document->product;
			
			if ( $product->HasAMasterShapeRepresentation() ) {
				$this->shape['filePath'] = $product->GetMasterShapeRepresentationPathName($activate);
				$pathInfos = pathinfo($this->shape['filePath']);
				$this->shape['name'] = $pathInfos['basename'];
				$this->shape['partNumber'] = $pathInfos['filename'];
			}
		}
		return $this->shape;
	}

	/**
	 */
	public function normalizeView()
	{
		$catApplication = $this->catia->getApplication();
		
		$window = $catApplication->ActiveWindow;
		$myViewer = $window->ActiveViewer;
		
		$myViewer->PutBackgroundColor(array(
			1,
			1,
			1
		));
		$myViewer->RenderingMode = CatRenderingMode::catRenderShadingWithEdges;
		$window->Layout = CatSpecsAndGeomWindowLayout::catWindowGeomOnly;
		
		/* Put in iso view */
		$myViewpoint = $myViewer->Viewpoint3D;
		$myViewpoint->PutOrigin(Array(
			0,
			0,
			0
		));
		$myViewpoint->PutSightDirection(Array(
			-0.577,
			-0.577,
			-0.577
		));
		$myViewpoint->PutUpDirection(Array(
			0,
			0,
			1
		));
		$myViewer->Reframe;
		
		$this->viewer = $myViewer;
		$this->window = $window;
		return $this;
	}

	/**
	 *
	 * @return \Rbcs\Converter\CatpartToRanchbe
	 */
	public function toJpg($file)
	{
		/* Prepare view */
		$this->normalizeView();
		
		/* Format unix path to windows path */
		$file = str_replace('/', '\\', $file);
		
		/* See enum type CatCaptureFormat */
		$this->viewer->CaptureToFile(CatCaptureFormat::catCaptureFormatJPEG, $file);
		return $this;
	}

	/**
	 */
	public function toWrl($file)
	{
		$catiaDocument = $this->document;
		
		$settingControllers1 = $this->catia->getApplication()->SettingControllers;
		$vrmlSettingAtt1 = $settingControllers1->Item("CATVisVrmlSettingCtrl");
		$vrmlSettingAtt1->ExportVersion = 2; // VRML 97
		$vrmlSettingAtt1->SetExportBackgroundColor(255, 255, 255); // Background color
		
		/* Export to vrml file */
		$catiaDocument->ExportData($file, "wrl");
	}

	/**
	 */
	public function toStl($file)
	{
		$catiaDocument = $this->document;
		
		/* Format unix path to windows path */
		$file = str_replace('/', '\\', $file);
		
		$catiaDocument->ExportData($file, "stl");
		return $this;
	}

	/**
	 */
	public function toIgs($file)
	{
		$catiaDocument = $this->document;
		
		$settingControllers1 = $this->catia->getApplication()->SettingControllers;
		$settingAtt1 = $settingControllers1->Item("CATIdeIgesSettingCtrl");
		$settingAtt1->ApplyJoin = 1;
		$settingAtt1->ImportGroupAsSelSet = 1;
		$settingAtt1->ExportMSBO = 1;
		$settingAtt1->SaveRepository();
		
		/* Format unix path to windows path */
		$file = str_replace('/', '\\', $file);
		
		$catiaDocument->ExportData($file, "igs");
		return $this;
	}
}