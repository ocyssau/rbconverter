<?php
namespace Rbcs\Catia;

class Application extends \Com\Connector
{

	/**
	 * 
	 * @var \variant
	 */
	public $activeDocument;

	/**
	 */
	public function __construct()
	{
		$this->comApplicationName = 'CATIA';
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Com\Connector::connect()
	 */
	public function connect()
	{
		parent::connect();
		$this->application->Interactive = false;
		$this->application->RefreshDisplay = false;
		$this->application->Visible = true;
		$this->application->Height = 1;
		$this->application->Width = 300;
		$this->application->Top = 0;
		$this->application->Left = 0;
		$this->application->Caption = "RANCHBE CONVERTER";
		$this->application->DisplayFileAlerts = false;
		$this->application->Interactive = false;
		return $this->application;
	}

	/**
	 *
	 * @return array
	 */
	public function getVersion()
	{
		$system = $this->application->SystemConfiguration;
		$cat['version'] = $system->Version;
		$cat['release'] = $system->Release;
		$cat['servicePack'] = $system->ServicePack;
		$cat['operatingSystem'] = $system->OperatingSystem;
		return $cat;
	}

	/**
	 *
	 * @param string $file        	
	 * @return Application
	 */
	public function openDocument($file)
	{
		$documents = & $this->application->Documents;
		$documentName = basename($file);
		
		try {
			$this->activeDocument = $documents->Item($documentName);
		}
		catch( \Exception $e ) {
			$this->activeDocument = $documents->Open($file);
			$this->activeDocument = $this->application->ActiveDocument();
		}
		return $this;
	}

	/**
	 * Reads a document stored in a file.
	 * This method has to be used only for Browse purpose, for instance to retrieve Product properties.
	 * Be careful, it doesn't open any editor (no visualization, no undo/redo capabilities...)
	 *
	 * @param string $file        	
	 * @return Application
	 */
	public function readDocument($file)
	{
		$documents = & $this->application->Documents;
		$documentName = basename($file);
		
		try {
			$this->activeDocument = $documents->Item($documentName);
		}
		catch( \Exception $e ) {
			$this->activeDocument = $documents->Read($file);
		}
		return $this;
	}

	/**
	 *
	 * @param string $file        	
	 * @return Application
	 */
	public function closeDocument($document)
	{
		$document->close();
		return $this;
	}

	/**
	 * @return \variant
	 */
	public function getActiveDocument()
	{
		return $this->application->ActiveDocument;
	}

	/**
	 * @return Application
	 */
	public function closeAllDocuments()
	{
		foreach( $this->application->Documents as $openDoc ) {
			$openDoc->Close();
		}
		return $this;
	}
	
	/**
	 * @return Application
	 */
	public function quit()
	{
		$this->application->quit;
		return $this;
	}
}
