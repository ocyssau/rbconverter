<?php
namespace Rbcs\Catia;

/*
 * Sub myCallback() On Error GoTo 0 On Error GoTo 0 On Error GoTo 0 On Error GoTo 0 On Error GoTo 0 On Error GoTo 0 On Error GoTo 0 'On Error GoTo Desactivated 'Set Params = Me.MyParentProduct.Parameters 'b = Me.MyParentProduct.PartNumber & "\" & Me.MyInstanceProduct.name & "\Component Activation State" 'Acti = Params.GetItem(b).Value 'If (Acti = False) Then ' Exit Sub 'End If 'Filter ENV Nodes Set RegEx = CreateObject("vbscript.regexp") With RegEx .MultiLine = False .Global = True .IgnoreCase = True End With RegEx.pattern = "^ENV" ' create the collection of matches 'Set RegMatchCollection = RegEx.Execute(Me.MyInstanceProduct.Name) Set RegMatchCollection = RegEx.Execute(Me.MyInstanceProduct.PartNumber) If RegMatchCollection.Count > 0 Then GoTo Desactivated End If RegEx.pattern = "ENV-" ' create the collection of matches Set RegMatchCollection = RegEx.Execute(Me.MyInstanceProduct.PartNumber) If RegMatchCollection.Count > 0 Then GoTo Desactivated End If End Sub
 */

/**
 */
class ProductDocument
{

	/**
	 *
	 * @var \stdClass
	 */
	protected $document;

	/**
	 *
	 * @var \stdClass
	 */
	protected $product;

	/**
	 *
	 * @var string
	 */
	protected $fullName;

	/**
	 *
	 * @var string
	 */
	protected $name;

	/**
	 *
	 * @var string
	 */
	protected $path;

	/**
	 *
	 * @var boolean
	 */
	protected $readonly;

	/**
	 *
	 * @var boolean
	 */
	protected $saved;

	/**
	 *
	 * @var \stdClass
	 */
	protected $catia;

	/**
	 *
	 * @param \variant $catDocument
	 *        	COMCATIADocument
	 */
	public function __construct($catDocument, $catia)
	{
		$this->document = $catDocument;
		$this->catia = $catia;
		$this->product = $catDocument->product;
		
		$this->fullName = $catDocument->FullName;
		$this->name = $catDocument->Name;
		$this->path = $catDocument->Path;
		$this->readOnly = $catDocument->ReadOnly;
		$this->saved = $catDocument->Saved;
	}

	/**
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		$output = array(
			'fullName' => $this->fullName,
			'name' => $this->name,
			'number' => $this->name,
			'path' => $this->path,
			'readonly' => $this->readOnly,
			'saved' => $this->saved,
			'product' => array(
				'name' => $this->product->Name,
				'number' => $this->product->PartNumber,
				'nomenclature' => $this->product->Nomenclature,
				'version' => $this->product->Revision,
				'definition' => $this->product->Definition,
				'description' => $this->product->DescriptionRef
			)
		);
		
		return $output;
	}

	/**
	 */
	public function getChildren($parent = null)
	{
		if ( !isset($this->children) ) {
			$this->children = ProductWalker::getChildren($this->product, $this->document);
		}
		return $this->children;
	}

	/**
	 * 
	 * @param string $file
	 */
	public function toStep($file)
	{
		$catiaDocument = $this->document;
		
		$settingControllers1 = $this->catia->getApplication()->SettingControllers;
		$stepSettingAtt1 = $settingControllers1->Item("CATSdeStepSettingCtrl");
		
		$stepSettingAtt1->AttHeaderAuthor = 'RbConverter';
		$stepSettingAtt1->AttHeaderOrganisation = 'SIER';
		/* AP214 ISO */
		$stepSettingAtt1->AttAP = 2;
		/* Assemblage with links to catia */
		$stepSettingAtt1->AttASM = 3;
		/* unit mm */
		$stepSettingAtt1->AttUnits = 0;
		/* export no show */
		$stepSettingAtt1->AttShow = 1;
		/* export not visibles layers */
		$stepSettingAtt1->AttLayersFilters = 1;
		
		/* Export to step file */
		$catiaDocument->ExportData($file, "stp");
	}

	/**
	 * 
	 * @return multitype:multitype:unknown NULL
	 */
	public function getProductInstanceLinks()
	{
		$ret = array();
		$catiaDocument = $this->document;
		$catiaProduct = $this->product;
		$catia = $catiaDocument->Application;
		$CAIEngine = $catia->GetItem("CAIEngine");
		$oStiDBItem = $CAIEngine->GetStiDBItemFromAnyObject($catiaDocument);
		$oStiDBChildren = $oStiDBItem->GetChildren();
		
		for ($i = 1; $i <= $oStiDBChildren->Count(); $i++) {
			$oStiDBItem2 = $oStiDBChildren->Item($i);
			$isComponent = $oStiDBItem2->IsCFOType;
			$isRoot = $oStiDBItem2->isRoot;
			$linkType = $oStiDBChildren->LinkType($i);
			
			$sonName = $oStiDBItem2->GetDocumentFullPath;
			$myProduct = $catiaProduct->Products->Item($i);
			$ret[] = array(
				'isComponent' => $isComponent,
				'isRoot' => $isRoot,
				'linType' => $linkType,
				'lnk' => $sonName,
				'instance' => $myProduct->name
			);
		}
		
		return $ret;
	}
}

