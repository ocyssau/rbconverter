<?php
namespace Rbcs\Catia;

/**
 */
class DrawingSheet
{

	public $sheet;

	public $format;

	public $name;

	/**
	 *
	 * @param \stdClass $catSheet        	
	 */
	public function __construct($catSheet)
	{
		$this->sheet = $catSheet;
		$this->format = $catSheet->PaperName;
		$this->name = $catSheet->Name;
		$this->orientation = $catSheet->Orientation;
	}

	/**
	 */
	public function getArrayCopy()
	{
		$output = array(
			'name' => $this->name,
			'format' => $this->format,
			'orientation' => $this->orientation
		);
		return $output;
	}
}
