<?php
namespace Rbcs\Catia;

/**
 */
class DrawingDocument
{

	public $document;

	public $fullName;

	public $name;

	public $path;

	public $readonly;

	public $saved;

	/**
	 *
	 * @var array
	 */
	protected $sheets;

	/**
	 *
	 * @param \stdClass $catDocument
	 */
	public function __construct($catDocument, $catia)
	{
		$this->document = $catDocument;
		$this->catia = $catia;
		
		$this->fullName = $catDocument->FullName;
		$this->name = $catDocument->Name;
		$this->path = $catDocument->Path;
		$this->readOnly = $catDocument->ReadOnly;
		$this->saved = $catDocument->Saved;
	}

	/**
	 *
	 * @return array
	 */
	public function loadSheets()
	{
		if ( !isset($this->sheets) ) {
			$sheets = $this->document->Sheets;
			for ($i = 1; $i < $sheets->Count + 1; $i++) {
				$this->sheets[] = new DrawingSheet($sheets->item($i));
			}
		}
		return $this->sheets;
	}

	/**
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		$output = array(
			'fullName' => $this->fullName,
			'name' => $this->name,
			'number' => $this->name,
			'path' => $this->path,
			'readonly' => $this->readOnly,
			'saved' => $this->saved,
			'sheets' => array()
		);
		
		foreach( $this->sheets as $sheet ) {
			$output['sheets'][$sheet->name] = $sheet->getArrayCopy();
		}
		
		return $output;
	}

	/**
	 */
	public function toPdf($file)
	{
		$catiaDocument = $this->document;
		$catiaDocument->ExportData($file, "pdf");
	}
}