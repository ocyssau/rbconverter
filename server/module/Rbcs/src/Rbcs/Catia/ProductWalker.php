<?php
namespace Rbcs\Catia;

use Rbcs\Catia\Exception\ReferenceProductLoadingException;

/**
 */
class ProductWalker
{

	/**
	 */
	public static function getChildren($product, $document, $path = null, $depth = 1)
	{
		$itemCounter = 1;
		$children = array();
		$errors = array();
		
		foreach( $product->Products as $childProduct ) {
			try {
				$instance = new ProductInstance($path . '/' . $itemCounter, $document);
			}
			catch( ReferenceProductLoadingException $e ) {
				$errors[] = "Error during loading of $childProduct->Name." . $e->getMessage();
			}
			catch( \Exception $e ) {
				$errors[] = sprintf('The product %s can not be load. Is ignored', $childProduct->Name);
			}
			
			$ret = $instance->getArrayCopy();
			
			/* Its a component ? */
			if ( $ret['ofDocumentName'] == $document->Name ) {
				$ret['children'] = ProductWalker::getChildren($childProduct, $document, $path . '/' . $itemCounter, $depth + 1);
			}
			
			$children[] = $ret;
			
			$itemCounter++;
		}
		
		/* If error attach result to exception */
		if ( $errors ) {
			$e = new ProductWalker\Exception(implode('; ', $errors));
			$e->children = $children;
			throw $e;
		}
		
		return $children;
	}
}

