<?php
namespace Rbcs\Catia;

use Rbcs\Catia\Exception\ReferenceProductLoadingException;

/**
 */
class ProductInstance
{

	/**
	 *
	 * @var \stdClass
	 */
	public $application;

	/**
	 *
	 * @var \stdClass
	 */
	public $product;

	/**
	 *
	 * @var string
	 */
	public $name;

	/**
	 *
	 * @var integer
	 */
	public $ofProductId;

	/**
	 *
	 * @var string
	 */
	public $ofProductUid;

	/**
	 *
	 * @var string
	 */
	public $ofProductName;
	
	/**
	 *
	 * @var string
	 */
	public $ofProductNumber;

	/**
	 *
	 * @var string
	 */
	public $ofDocumentName;

	/**
	 *
	 * @var string
	 */
	public $nomenclature;

	/**
	 *
	 * @var string
	 */
	public $quantity;

	/**
	 *
	 * @var string
	 */
	public $description;

	/**
	 *
	 * @var array
	 */
	public $position;

	/**
	 *
	 * @var array
	 */
	public $positionPrototype = array(
		'ux' => null,
		'uy' => null,
		'uz' => null,
		'vx' => null,
		'vy' => null,
		'vz' => null,
		'wx' => null,
		'wy' => null,
		'wz' => null,
		'x' => null,
		'y' => null,
		'z' => null
	);

	/**
	 *
	 * @param string $path
	 *        	String as id/id/id wheres id are Item index of the parents in Products relations
	 * @param ProductDocument $rootDocument        	
	 */
	public function __construct($path, $rootDocument)
	{
		$this->application = $rootDocument->Application;
		$parent = $rootDocument->Product->Products;
		$strPath = $path;
		$path = explode('/', trim($path, '/'));
		foreach( $path as $itemId ) {
			if ( $itemId == '' ) continue;
			$catInstance = $parent->Item((int)$itemId);
			$parent = $catInstance->Parent;
		}
		
		$this->path = $path;
		$this->rootDocument = $rootDocument;
		
		$this->name = $catInstance->Name;
		$this->nomenclature = $catInstance->Nomenclature;
		$this->description = $catInstance->DescriptionInst;
	}

	/**
	 *
	 * @throws ReferenceProductLoadingException
	 */
	public function loadReferenceProduct()
	{
		try {
			$this->product = $catInstance->ReferenceProduct;
			$this->document = $this->product->Parent;
			$this->ofProductName = $this->product->Name;
			$this->ofProductNumber = $this->product->Name;
			$this->ofDocumentName = $this->document->Name;
		}
		catch( \Exception $e ) {
			throw new ReferenceProductLoadingException(sprintf("The reference product can not be load. Probably because the file %s is unreachable by SearchOrder settings", ''));
		}
	}

	/**
	 */
	public function getArrayCopy()
	{
		$output = array(
			'name' => $this->name,
			'ofProductId' => $this->ofProductId,
			'ofProductUid' => $this->ofProductUid,
			'ofProductName' => $this->ofProductName,
			'ofProductNumber' => $this->ofProductName,
			'ofDocumentName' => $this->ofDocumentName,
			'nomenclature' => $this->nomenclature,
			'quantity' => $this->quantity,
			'description' => $this->description,
			'position' => $this->getPosition()
		);
		return $output;
	}

	/**
	 * Get the position matrix of the product item
	 * to get the product in the activeProduct
	 * If $itemId is array, construct path to object like this :
	 * Set Product = ActiveProduct.Products.item(1).Products.item(1) .
	 *
	 * ..etc for each value in itemPath
	 *        	
	 */
	public function getPosition()
	{
		if ( isset($this->position) ) {
			return $this->position;
		}
		
		$this->position = $this->positionPrototype;
		$catia = $this->application;
		
		$instanceName = $this->name;
		$rootDocumentName = $this->rootDocument->Name;
		$itemPath = $this->path;
		
		if ( is_array($itemPath) ) {
			foreach( $itemPath as $itemId ) {
				$productPath[] = 'Products.Item(' . $itemId . ')';
			}
			$productSet = 'Set instance = document.Product.' . implode('.', $productPath);
		}
		else {
			$productSet = 'Set instance = document.Product.Products.Item("' . $instanceName . '")';
		}
		
		$script = "
		Function GetPosition()
		    Dim instance As Variant
            Dim document As Document
            Set document = CATIA.Documents.Item(\"$rootDocumentName\")
		    $productSet
		    Dim PositionMatrix(11)
		    instance.Position.GetComponents PositionMatrix
		    GetPosition = PositionMatrix
		End Function";
		
		$scriptLanguage = 0; // CATVBScriptLanguage
		$funcName = 'GetPosition';
		$parameters = array();
		$res = $catia->SystemService->Evaluate($script, $scriptLanguage, $funcName, $parameters);
		
		$keys = array_keys($this->position);
		foreach( $res as $k => $v ) {
			$positionMatrix[$keys[$k]] = $v;
		}
		
		$this->position = $positionMatrix;
		return $positionMatrix;
	}

	/**
	 * Enter description here .
	 * ..
	 */
	public function getRootDocument()
	{
		/* Ne fonctione pas avec les components si depth>1 */
		$this->rootDocument = $catInstance->Parent->Parent->Parent; /* Produts.Product.ProductDocument */
	}

	/**
	 */
	public function isActive()
	{
		if ( $this->product ) {
			$params = $this->product->Parameters;
			$b = $this->referenceProduct->PartNumber . "\\" . $this->name . "\Component Activation State";
			$acti = $params->GetItem($b)->Value;
			return $acti;
		}
		else {
			throw new \Exception('The reference product is not loaded');
		}
	}

	/**
	 */
	public function isComponent()
	{}
}
