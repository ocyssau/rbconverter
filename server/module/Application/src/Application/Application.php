<?php
namespace Application;

use Zend\View\Model\ViewModel;
use Application\View\Renderer\PhpRenderer;
use Zend\View\Resolver;

/**
 */
class Application
{

	const VER = '1.1';

	/* Current version */
	const BUILD = '%RBCS_BUILD%';

	/* Build number */
	const COPYRIGHT = '&#169;2009-2010 by Olivier CYSSAU';

	/* Copyright */

	/**
	 *
	 * @var Application
	 */
	protected static $instance;

	/**
	 *
	 * @var array
	 */
	protected $config;

	/**
	 *
	 * @var bool
	 */
	public $isService = false;

	/**
	 */
	public function __construct()
	{
		self::$instance = $this;
	}

	/**
	 *
	 * @return Application
	 */
	public static function get()
	{
		if ( !isset(self::$instance) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Get the version, build and copyright in array
	 *
	 * @return array
	 */
	public static function getVersion()
	{
		return array(
			'version' => self::VER,
			'build' => self::BUILD,
			'copyright' => self::COPYRIGHT
		);
	}

	/**
	 *
	 * @param array $array
	 * @return Application
	 */
	public function setConfig($array)
	{
		$this->config = $array;
		return $this;
	}

	/**
	 * Return the value of configuration constant value.
	 * $name may be under form prefix.value in lower case
	 * if $name is not set, return all configuration values.
	 *
	 * @param string $name
	 * @return string|array
	 */
	public function getConfig($name = null)
	{
		if ( $name ) {
			if ( isset($this->config[$name]) ) {
				return $this->config[$name];
			}
		}
		else {
			return $this->config;
		}
	}

	/**
	 * Set the route elements
	 * Set controller object
	 * Set serviceManager
	 * Set isService boolean
	 */
	public function predispatch()
	{
		/* Detect AJAX request */
		if ( ((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) ) {
			$this->isService = true;
		}
		else {
			isset($_REQUEST['isService']) ? $this->isService = $_REQUEST['isService'] : $this->isService = false;
		}

		$smConfigurator = new \Application\ServiceManagerConfigurator();
		$this->serviceManager = $smConfigurator->createServiceManager($this->getConfig());

		/* Controller */
		if ( isset($_SERVER['REDIRECT_URL']) ) {
			$redirectUrl = $_SERVER['REDIRECT_URL'];
		}
		elseif ( isset($_SERVER['PATH_INFO']) ) {
			$redirectUrl = $_SERVER['PATH_INFO'];
		}
		
		isset($_SERVER['REDIRECT_BASE']) ? $redirectBase = $_SERVER['REDIRECT_BASE'] : $redirectBase = '';
		$redirectUrl = trim(str_replace($redirectBase, '', $redirectUrl), '/');

		$r = explode('/', $redirectUrl);
		$actionName = strtolower($r[2]);
		$controllerName = ucfirst($r[1]);
		$moduleName = ucfirst($r[0]);

		($actionName == '') ? $actionName = 'index' : null;
		($controllerName == '') ? $controllerName = 'Index' : null;
		($moduleName == '') ? $moduleName = 'Application' : null;

		$this->route = [
			'module' => $moduleName,
			'controller' => $controllerName,
			'action' => $actionName
		];

		$includeFile = 'server/module/' . $moduleName . '/src/' . $moduleName . '/Controller/' . $controllerName . 'Controller.php';
		if ( is_file($includeFile) ) {
			$controllerClass = $moduleName . '\Controller\\' . $controllerName . 'Controller';
			$controller = new $controllerClass();
			$controller->setRoute($moduleName, $controllerName, $actionName);
			$controller->setBaseUrl(BASEURL);
			$controller->setApplication($this);
			$this->controller = $controller;
			$controller->preDispatch();
		}
		else {
			header("HTTP/1.0 404 Not Found");
			var_dump($module, $controllerName, $action, $controllerClass, $includeFile);
			die();
		}

		/* Set view renderer */
		$renderer = new PhpRenderer();
		$resolver = new Resolver\AggregateResolver();
		$renderer->setResolver($resolver);
		$stack = new Resolver\TemplatePathStack(array(
			'script_paths' => $this->config['view']['view_manager']['template_path_stack']
		));

		$resolver->attach($stack);
		$this->viewRenderer = $renderer;

		/* Set view plugins manager */
		$sm = $this->serviceManager;
		$viewHelpers = $sm->get('ViewHelperManager');
		$renderer->setHelperPluginManager($viewHelpers);

		return $this;
	}

	/**
	 *
	 * @return \Application\Application
	 */
	public function run()
	{
		$controller = $this->controller;
		$action = $this->route['action'];
		if ( $this->isService ) {
			try {
				header('Content-Type: text/json');

				$methodName = $action . 'Service';
				$return = call_user_func(array(
					$controller,
					$methodName
				));

				$controller->postDispatch();
				$controller->getRespons()->sendAsJson();
			}
			catch( \Exception $e ) {
				http_response_code(500);
				$controller->exceptionToJson($e);
			}
		}
		else {
			try {
				$layout = new ViewModel();
				$layout->setTemplate('layout/layout');

				$view = call_user_func(array(
					$controller,
					$action . 'Action'
				));
				$controller->postDispatch();
				$this->viewRenderer->setCanRenderTrees(true);
			}
			catch( Controller\ControllerException $e ) {
				$view = new ViewModel();
				$view->setTemplate('application/error');
				$view->message = $e->getMessage();
			}
			
			if ( $view instanceof ViewModel && isset($this->viewRenderer)) {
				$layout->addChild($view, 'content');
				echo $this->viewRenderer->render($layout);
			}
		}

		return $this;
	}
}
