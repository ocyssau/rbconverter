<?php
namespace Application\View\Renderer;

use Zend\View\Model\ModelInterface as Model;
use Zend\View\Renderer\TreeRendererInterface;
use Zend\View\Exception;

/**
 * Extends PhpRenderer to add children render
 */
class PhpRenderer extends \Zend\View\Renderer\PhpRenderer
{

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Zend\View\Renderer\PhpRenderer::render()
	 */
	public function render($model, $values = null)
	{
		// If we have children, render them first, but only if:
		// a) the renderer does not implement TreeRendererInterface, or
		// b) it does, but canRenderTrees() returns false
		if ( $model->hasChildren() ) {
			$this->renderChildren($model);
		}
		
		$output = parent::render($model, $values = null);
		return $output;
	}

	/**
	 * Loop through children, rendering each
	 *
	 * @param Model $model        	
	 * @throws Exception\DomainException
	 * @return void
	 */
	protected function renderChildren(Model $model)
	{
		foreach( $model as $child ) {
			if ( $child->terminate() ) {
				throw new Exception\DomainException('Inconsistent state; child view model is marked as terminal');
			}
			$child->setOption('has_parent', true);
			$result = $this->render($child);
			$child->setOption('has_parent', null);
			$capture = $child->captureTo();
			if ( !empty($capture) ) {
				if ( $child->isAppend() ) {
					$oldResult = $model->{$capture};
					$model->setVariable($capture, $oldResult . $result);
				}
				else {
					$model->setVariable($capture, $result);
				}
			}
		}
	}
}
