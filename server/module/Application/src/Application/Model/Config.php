<?php
namespace Application\Model;

class Config
{

	/**
	 *
	 * @var array
	 */
	protected $config;

	/**
	 */
	public function __construct($config)
	{
		$this->config = array();
		foreach( $config as $key => $value ) {
			$key = str_replace('.', '_', $key);
			$this->config[$key] = $value;
		}
	}

	/**
	 *
	 * @param
	 *        	array
	 */
	public function populate($properties)
	{
		foreach( $properties as $key => $value ) {
			$this->config[$key] = $value;
		}
	}

	/**
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		return $this->config;
	}

	/**
	 *
	 * @param string $name        	
	 */
	public function __get($name)
	{
		return $this->config[$name];
	}

	/**
	 *
	 * @return array
	 */
	public function toConfig()
	{
		$config = array();
		foreach( $this->config as $key => $value ) {
			$key = str_replace('_', '.', $key);
			$config[$key] = $value;
		}
		return $config;
	}
}

