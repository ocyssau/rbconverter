<?php
namespace Application\Controller;

/**
 * 
 *
 */
class Respons implements \jsonSerializable
{

	protected $datas;

	protected $feedbacks;

	protected $errors;

	/**
	 *
	 * @param array $datas        	
	 */
	public function setDatas($name, $datas)
	{
		$this->datas[$name] = $datas;
	}

	/**
	 */
	public function setErrors($errors)
	{
		$this->errors = $errors;
	}

	/**
	 */
	public function setFeedbacks($fb)
	{
		$this->feedbacks = $fb;
	}

	/**
	 *
	 * @param string $msg        	
	 */
	public function error($msg)
	{
		$this->errors[] = (string)$msg;
	}

	/**
	 *
	 * @param string $msg        	
	 */
	public function feedback($msg)
	{
		$this->feedbacks[] = $msg;
	}

	/**
	 */
	public function getDatas()
	{
		return $this->datas;
	}

	/**
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 */
	public function getFeedback()
	{
		return $this->feedbacks;
	}

	public function jsonSerialize()
	{
		return array(
			'datas'=>$this->datas,
			'feedbacks'=>$this->feedbacks,
			'errors'=>$this->errors
		);
	}

	/**
	 */
	public function sendAsJson()
	{
		echo json_encode(array(
			'datas' => $this->datas,
			'feedbacks' => $this->feedbacks,
			'errors' => $this->errors
		));
	}
}
