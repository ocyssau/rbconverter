<?php
namespace Application\Controller;

/**
 * 
 *
 */
abstract class Controller
{

	/**
	 *
	 * @var \Application\Application
	 */
	protected $application;

	/**
	 */
	protected $request;

	/**
	 *
	 * @var array
	 */
	protected $route;

	/**
	 *
	 * @var string
	 */
	protected $baseurl;

	/**
	 *
	 * @var boolean
	 */
	protected $isService;

	/**
	 *
	 * @var Respons
	 */
	protected $respons;

	/**
	 *
	 * @var ErrorStack
	 */
	protected $errorStack;

	/**
	 */
	public function __construct()
	{
		$this->errorStack = new ErrorStack();
		$this->respons = new Respons();
	}

	/**
	 */
	public function preDispatch()
	{}

	/**
	 * To run after action
	 * Not called if redirect is call
	 */
	public function postDispatch()
	{}

	/**
	 *
	 * @return Respons
	 */
	public function getRespons()
	{
		return $this->respons;
	}

	/**
	 * Emulate Route component of ZF2
	 * 
	 * @param string $module
	 * @param string $controller
	 * @param string $action
	 */
	public function setRoute($module, $controller, $action)
	{
		$this->route[0] = $module;
		$this->route[1] = $controller;
		$this->route[2] = $action;
		if ( isset($this->view) ) {
			$this->view->assign('module', $module);
			$this->view->assign('controller', $controller);
			$this->view->assign('action', $action);
		}
	}

	/**
	 *
	 * @param
	 *        	string
	 * @param
	 *        	string
	 * @param
	 *        	string
	 */
	public function setBaseUrl($baseUrl)
	{
		$baseUrl = trim($baseUrl, '/');
		if ( $baseUrl ) {
			$this->baseurl = $baseUrl;
		}
		if ( isset($this->view) ) {
			$this->view->assign('baseurl', $baseUrl);
		}
	}

	/**
	 *
	 * @param \Application\Application $application
	 * @return \Application\Controller\Controller
	 */
	public function setApplication(\Application\Application $application)
	{
		$this->application = $application;
		return $this;
	}

	/**
	 */
	public function getRoute()
	{
		return strtolower($this->baseurl . '/' . $this->route[0] . '/' . $this->route[1] . '/' . $this->route[2]);
	}

	/**
	 *
	 * @param string $action
	 */
	public function actionUrl($action)
	{
		return strtolower($this->baseurl . '/' . $this->route[0] . '/' . $this->route[1] . '/' . $action);
	}

	/**
	 */
	public function getControllerRoute()
	{
		return strtolower($this->baseurl . '/' . $this->route[0] . '/' . $this->route[1]);
	}

	/**
	 */
	public function getModuleRoute()
	{
		return strtolower($this->baseurl . '/' . $this->route[0]);
	}

	/**
	 * $return is json encoded and send to respons.
	 * If errors is found in errorStack, then return the found errors as Json.
	 *
	 * @param array $return
	 */
	public function serviceReturn($return)
	{}

	/**
	 *
	 * @param \Exception $e
	 * @return string Json string
	 */
	public function exceptionToJson($e)
	{
		$return = array(
			'error' => array(
				'message' => $e->getMessage(),
				'code' => $e->getCode(),
				'file' => $e->getFile(),
				'line' => $e->getLine()
			)
		);
		echo json_encode($return);
	}

	/**
	 *
	 * @return \Application\Controller\ErrorStack
	 */
	public function errorStack()
	{
		return $this->errorStack;
	}
}
