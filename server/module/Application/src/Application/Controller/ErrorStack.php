<?php
namespace Application\Controller;

/**
 */
class ErrorStack
{

	/**
	 *
	 * @var array
	 */
	protected $errors;

	/**
	 *
	 * @var array
	 */
	protected $warnings;

	/**
	 *
	 * @param string $msg
	 */
	public function error($msg)
	{
		$this->errors[] = $msg;
	}

	/**
	 *
	 * @param string $msg
	 */
	public function warning($msg)
	{
		$this->warnings[] = $msg;
	}

	/**
	 *
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 *
	 * @return array
	 */
	public function getWarnings()
	{
		return $this->warnings;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasError()
	{
		return isset($this->errors);
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasWarning()
	{
		return isset($this->warnings);
	}
}
