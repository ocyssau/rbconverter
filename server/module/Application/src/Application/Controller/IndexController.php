<?php
namespace Application\Controller;

use Zend\View\Model\ViewModel;

/**
 */
class IndexController extends \Application\Controller\Controller
{

	/**
	 */
	public function pingService()
	{
		echo __METHOD__ . CRLF;
		return $this->return;
	}

	/**
	 */
	public function indexAction()
	{
		$view = new ViewModel();
		$view->setTemplate('application/index/index');
		$view->pageTitle = __METHOD__;
		return $view;
	}
}
