<?php
namespace Com;

class Connector
{

	/**
	 * @var \COM
	 * Reference to Com Application
	 */
	public $application;

	/**
	 * The name of the COM server without suffix ".Application"
	 * 
	 * @var string
	 */
	protected $comApplicationName;

	/**
	 *
	 * @param string $name        	
	 */
	public function __construct($name)
	{
		$this->comApplicationName = $name;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->comApplicationName;
	}

	/**
	 * @return \COM
	 */
	public function getApplication()
	{
		return $this->application;
	}

	/**
	 * @return \COM
	 */
	public function connect()
	{
		if ( !$this->application ) {
			$this->application = new \COM($this->comApplicationName . '.Application');
		}
		$this->application->visible = true;
		return $this->application;
	}
}
