<?php
namespace Soapserver\Transport;

/**
 * 
 *
 */
class Ftp implements TransportInterface
{

	private $_workingfile;

	private $workingDir;

	/**
	 *
	 * @param string $workingDir        	
	 * @throws \SoapFault
	 */
	public function __construct($workingDir)
	{
		if ( is_dir($workingDir) ) {
			$this->workingDir = $workingDir;
		}
		else {
			throw new \SoapFault("Server", "4000| $workingDir is not a directory");
		}
	}

	/**
	 */
	public function getResult(\Rbcs\Converter\ConverterInterface $converter)
	{
		/* clean the working directory */
		unlink($converter->getInputFile());
		// unlink( $converter->getOutputFile() );
		
		return array(
			'input_file' => $converter->getInputFile(),
			'output_file' => $converter->getOutputFile()
		);
	}
} /* End of class */
