<?php
namespace Soapserver\Transport;

/**
 */
class Directory implements TransportInterface
{

	private $workingDir;

	/**
	 *
	 * @param string $workingDir        	
	 * @throws \SoapFault
	 */
	public function __construct($workingDir)
	{
		if ( is_dir($workingDir) ) {
			$this->workingDir = $workingDir;
		}
		else {
			throw new \SoapFault("Server", "4000| $workingDir is not a directory");
		}
	}

	/**
	 * 
	 * @param \Rbcs\Converter\ConverterInterface $converter
	 * @return multitype:NULL
	 */
	public function getResult(\Rbcs\Converter\ConverterInterface $converter)
	{
		/* clean the working directory */
		unlink($converter->getInputFile());
		// unlink( $converter->getOutputFile() );
		
		return array(
			'input_file' => $converter->getInputFile(),
			'output_file' => $converter->getOutputFile()
		);
	}

} /* End of class */
