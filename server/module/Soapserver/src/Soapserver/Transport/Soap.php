<?php
namespace Soapserver\Transport;

use Rbcs\Converter\ConverterInterface;

/**
 * 
 *
 */
class Soap implements TransportInterface
{

	private $workingfile;

	private $workingDir;

	/**
	 *
	 * @param string $workingDir        	
	 * @throws \SoapFault
	 */
	public function __construct($workingDir)
	{
		if ( is_dir($workingDir) ) {
			$this->workingDir = $workingDir;
		}
		else {
			throw new \SoapFault("Server", "4000| $workingDir is not a directory");
		}
	}
		  
	/**
	 * Write attachment content in file on disk
	 * 
	 * @param array $option        	
	 * @return string path to file
	 * @throws \SoapFault
	 */
	public function getFile(array $options)
	{
		$this->workingfile = tempnam($this->workingDir, 'rbConverter');
		
		if ( !$options['data'] ) return false;
		
		$ret = ($f = fopen($this->workingfile, "w")) && (fputs($f, base64_decode($options['data'])) !== false) && fclose($f);
		@chmod($this->workingfile, 0666);
		
		if ( !$ret ) {
			throw new \SoapFault("Server", "2005| Unable to write a temporary file");
		}
		
		return $this->workingfile;
	}
	  
	/**
	 * (non-PHPdoc)
	 * 
	 * @see server/library/Rbcs/Transport/Rbcs_Transport_Interface#getResult($converter)
	 */
	public function getResult(ConverterInterface $converter)
	{
		// encode content of file
		$outputfile = $converter->getOutputFile();
		$inputfile = $converter->getInputFile();
		
		$handle = fopen($outputfile, "rb");
		if ( !$handle ) {
			throw new \SoapFault("Server", "Unable to open resultfile $outputfile of input file $inputfile");
		}
		$contents = fread($handle, filesize($outputfile));
		fclose($handle);
		
		/* clean the working directory */
		unlink($this->workingfile);
		unlink($outputfile);
		
		return array(
			'input_file' => $inputfile,
			'output_file' => $outputfile,
			'data' => base64_encode($contents)
		);
	}
} /* End of class */
