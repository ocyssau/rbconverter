<?php
namespace Soapserver\Transport;

/**
 * 
 * @author olivier
 *
 */
interface TransportInterface
{
	/**
	 *
	 * @param \Rbcs\Converter\ConverterInterface $converter        	
	 * @return multitype:NULL
	 */
	public function getResult(\Rbcs\Converter\ConverterInterface $converter);
}
