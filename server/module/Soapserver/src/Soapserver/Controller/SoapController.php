<?php

namespace Soapserver;

/**
 * 
 *
 */
class IndexController extends \Application\Controller\Controller
{

	/**
	 *
	 * @var array
	 */
	private $_soapConf;

	/**
	 *
	 * @var string
	 */
	private $_wsdlUri;

	/**
	 * 
	 */
	public function init()
	{
		//$this->_soapConf = Zend_Registry::getInstance()->configuration->soap->toArray();
		//$this->_wsdlUri = Zend_Registry::getInstance()->configuration->wsdl->uri;
	}

	/**
	 * 
	 */
	public function testAction()
	{
		echo 'Yeeepeee, that all right';
	}

	/**
	 * 
	 */
	public function indexAction()
	{
		if ( isset($_GET['wsdl']) ) return $this->_getWsdl();
		
		/*
		 * Authenticate if require
		 * if(Zend_Registry::getInstance()->configuration->useSecurity){
		 * if (!Zend_Auth::getInstance()->hasIdentity()) {
		 * return $this->_auth();
		 * }
		 * }
		 */
		
		$server = new \SoapServer($this->_wsdlUri, $this->_soapConf);
		
		$server->setClass('Rbcs_Converter');
		$server->setPersistence(SOAP_PERSISTENCE_REQUEST);
		$server->handle();
		die();
	}

	/**
	 * 
	 */
	public function _getWsdl()
	{
		//$autodiscover = new Zend_Soap_AutoDiscover();
		$autodiscover->setClass('Rbcs_Converter');
		$autodiscover->handle();
		die();
	}
} /* End of class */
