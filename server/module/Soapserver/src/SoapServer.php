<?php
namespace Soapserver;

/**
 * Rbcs\Converter is the main class of the soap server.
 * This class is used to defined
 * the wsdl file.
 * Only public method are published by th soap server
 */
class Soapserver
{

	/**
	 * Full path to file to convert with extension and file name
	 *
	 * @var string
	 */
	private $fromFile;

	/**
	 * Full path to result file with extension and file name
	 *
	 * @var string
	 */
	private $toFile;

	/**
	 * Base 64 encoded data to convert
	 *
	 * @var string base64
	 */
	private $data;

	/**
	 * Type of original file: odt,doc...
	 *
	 * @var string
	 */
	private $fromMtype;

	/**
	 * Type of result file
	 *
	 * @var string
	 */
	private $toMtype;

	/**
	 *
	 * @var \Rbcs\Converter\ConverterInterface
	 */
	private $converter;

	/**
	 * Directory where is original file and converted file
	 *
	 * @var string
	 */
	private $workingDir;

	/**
	 * Array of transports used
	 *
	 * @var array
	 */
	private $transport = array(
		'Ftp' => false,
		'Directory' => false,
		'Soap' => false
	);

	/**
	 * Default transport type ; Soap, Ftp, Directory
	 *
	 * @var string
	 */
	private $transportType = 'Soap';

	/**
	 */
	public function __construct($config)
	{
		$this->workingDir = $config['working_dir'];
		$this->config = $config;
	}

	/**
	 * $option['from_mtype'] //original type
	 * $option['to_mtype'] //type of result file
	 * $option['from_file'] //file name of original file.
	 * This file must be in working directory
	 * $option['to_file'] //file name of result file will be put in working directory
	 */
	protected function _init($options)
	{
		$this->fromMtype = strtolower($options['from_mtype']);
		$this->toMtype = $options['to_mtype'];
		
		if ( $options['to_file'] ) {
			$this->toFile = $this->workingDir . '/' . basename($options['to_file']);
		}
		else {
			$this->toFile = tempnam($this->workingDir, 'rbconverter');
		}
		
		if ( $options['data'] ) {
			$this->transportType = 'Soap';
		}
		else {
			$this->transportType = 'Ftp';
		}
		$this->fromFile = $this->_getTransport($this->transportType)->getFile($options);
		
		$ret = array(
			'error' => false
		);
		
		if ( !is_file($this->fromFile) ) {
			throw new \SoapFault("Server", "1001| $this->fromFile is not a file");
		}
		
		if ( !$this->toMtype ) {
			throw new \SoapFault("Server", "1002| Option to_mtype is not set");
		}
		
		if ( !$this->fromMtype ) {
			throw new \SoapFault("Server", "1003| Option from_mtype is not set");
		}
		
		if ( !$this->toFile ) {
			throw new \SoapFault("Server", "1004| Option to_file is not set");
		}
		
		return $ret;
	}

	/**
	 * Convert method
	 *
	 * @param Array $options        	
	 * @return Array
	 */
	public function convert(array $options)
	{
		$ret = $this->_init($options);
		
		$this->_setConverterClass($this->fromMtype, $this->toMtype);
		$this->converter = new $this->converterClass();
		
		$this->converter->setInputFile($this->fromFile);
		$this->converter->setOutputExtension($this->toMtype);
		$this->converter->setOutputFile($this->toFile);
		
		$transportType = $this->transportType;
		if ( isset($options['return_by']) ) {
			$transportType = $options['return_by'];
		}
		
		if ( $this->converter->convert() ) {
			return $this->_getTransport($transportType)->getResult($this->converter);
		}
		else {
			throw new \SoapFault("Server", '1000| ' . $this->converter->getError() . ' Conversion failed');
		}
	}

	/**
	 * getError method
	 *
	 * @return Array
	 */
	public function getError()
	{
		return $this->converter->getError();
	}

	/**
	 *
	 * @param string $type        	
	 * @throws \SoapFault
	 */
	protected function _getTransport($type)
	{
		$type = strtolower($type);
		$config = $this->config;
		if ( !$config['transports'][$type] ) {
			throw new \SoapFault("Server", "This transport mode is not accepted by this server");
		}
		
		$type = ucfirst($type);
		
		if ( !$this->transport[$type] ) {
			$class = '\Rbcs\Transport\\' . $type;
			$this->transport[$type] = new $class($this->workingDir);
		}
		
		return $this->transport[$type];
	}

	/**
	 *
	 * @param string $fromMtype        	
	 * @param string $toMtype        	
	 */
	protected function _setConverterClass($fromMtype, $toMtype)
	{
		return $this->converterClass = '\Rbcs\Converter\\' . ucfirst($fromMtype . $toMtype);
	}

	/**
	 *
	 * @param string $username        	
	 * @param string $password        	
	 * @return void
	 */
	public function headerAuthentify($username, $password)
	{
		try {
			//$authconfig = \Zend\Registry::getInstance()->configuration->auth;
			
			if ( $authconfig->useSecurity == 0 ) {
				return;
			}
			
			//$adaptateur = new Zend_Auth_Adapter_Digest($authconfig->digestFileName, $authconfig->adapter->realm, $username, md5($password));
		}
		catch( \SoapFault $e ) {
			throw new \SoapFault("Server", $e);
		}
		
		$resultat = $adaptateur->authenticate();
		if ( !$resultat->isValid() ) {
			// throw new SoapFault("Server", md5($password) . " $username $password $authconfig->digestFileName $realm ");
			throw new \SoapFault("Server", "Authenticate failed. You must provide a username and password in soap header. Consult RbService documentation.");
		}
		
		return;
	}
} /* End of class */
