<?php

class Core_Request{

	private $_file;
	private $_file_extension;
	private $_file_path;
	private $_file_basename;
	private $_file_name;
	private $_toType;
	private $_toFile;
	private $_ftpconnexion;

	/**
	 */
	public function __construct($file, $toType, $configuration){

		//file infos
		$path_infos = pathinfo($file);
		$this->_file = $file;
		$this->_file_extension = strtolower($path_infos['extension']);
		$this->_file_path = $path_infos['dirname'];
		$this->_file_basename = $path_infos['basename'];
		$this->_file_name = $path_infos['filename'];
		$this->_toType = $toType;
		$this->_toFile = md5_file($this->_file).'.'.$this->_file_extension;

		//Select the application
		$converterName = $this->_file_extension.$this->_toType;
		$application = $configuration->application->$converterName;

		$this->_conf = $configuration->$application;

	}

	public function convert() {

		//Send file to conversion server
		//if(!$this->_ftp_put($this->_file, $this->_toFile)) return false;

		use_soap_error_handler(true);
		//Send soap request to server
		$service = new SoapClient($this->_conf->wsdl->uri, $this->_conf->soap->toArray());

		//$options['from_file']  = $this->_toFile;
		//$options['to_file']    = $this->_toFile.'.'.$this->_file_extension;
		$handle = fopen($this->_file, "rb");
		$options['data'] = base64_encode( fread($handle, filesize($this->_file)) );
		$options['from_mtype'] = $this->_file_extension;
		$options['to_mtype']   = $this->_toType;
		$options['return_by'] = 'soap';

		$res = $service->convert( $options );
		if($ret['data']){
			$data = base64_decode($ret['data']);
			$to_file = 'c:/tmp/'.basename($this->_toFile);
			$fp = fopen($to_file, "a");
			fputs($fp, $data);
		}

		if($res['error']){
			//Close ftp connexion
			//ftp_close($this->_ftpconnexion);
			return $res;
		}

		//Get converted file from ftp server
		//$this->_ftp_get(dirname($this->_file).'/'.$res['output_file'], $res['output_file']);

		//Close ftp connexion
		//ftp_close($this->_ftpconnexion);

		return $res;

	}

	public function test() {
		$service = new SoapClient($this->_conf->wsdl->uri, $this->_conf->soap->toArray());
		return $service->test();
	}

	public function _getFtpConnexion() {
		if(!$this->_ftpconnexion){
			//Connect to ftp server
			if(!$this->_ftpconnexion = ftp_connect($this->_conf->ftp->host, $this->_conf->ftp->port)){
				echo "Connexion to ftp server failed \n";
				return false;
			}
			//Authenticate on ftp server
			ftp_login($this->_ftpconnexion, $this->_conf->ftp->user, $this->_conf->ftp->passwd);
		}

		return $this->_ftpconnexion;
	} 

	public function _ftp_put($localfile, $remotefile) {
		//Connect to ftp server
		$ftp =& $this->_getFtpConnexion();

		//Put file on ftp server
		if (!ftp_put( $ftp, $remotefile, $localfile, FTP_BINARY)) {
			echo "Il y a eu un probl�me lors du chargement du fichier $localfile\n";
			return false;
		}

		return true;
	}

	public function _ftp_get($localfile, $remotefile) {

		//Connect to ftp server
		$ftp =& $this->_getFtpConnexion();

		//Put file on ftp server
		if (!ftp_get( $ftp, $localfile, $remotefile, FTP_BINARY)) {
			echo "Il y a eu un probl�me lors du chargement du fichier $remotefile\n";
			return false;
		}

		/*
		 //delete the remote file from server
		if (ftp_delete($ftp, $remotefile)) {
		echo "$remotefile is deleted from server\n";
		} else {
		echo "Delete of file $remotefile failed \n";
		}
		*/

		return true;

	}

} /* End of class */
