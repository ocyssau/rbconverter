<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

define('APPLICATION_PATH',  str_replace('\\','/',realpath(dirname(__FILE__) . '/..') ) );

/* Class to create soap type to transmit user name and password to soap server in soap header
 *
*/
class UserPass
{
	public $Username;
	public $Password;

	function __construct($Username, $Password)
	{
		$this->Username = $Username;
		$this->Password = $Password;
	}
}
	
//Create a soap var from UserPass class
$soapAuthenticator = new SoapVar(
		new UserPass("user", "user00"),
		SOAP_ENC_OBJECT,
		"UserPass");
	
//Create header. This header will be put in parameters of method headerAuthentify of the soap server
$authHeader = new SoapHeader("http://schemas.xmlsoap.org/ws/2002/07/utility","headerAuthentify",$soapAuthenticator,false);

try {
	ini_set("soap.wsdl_cache_enabled", 0);
	ini_set("soap.wsdl_cache_ttl", 0);

	$from_file = realpath(APPLICATION_PATH . '/../samples/test.doc');
	$to_file = 'c:/tmp/test.pdf';

	$wsdluri = "http://localhost/rbconverter/?wsdl";
	$soapConfig = array(
			'cache_wsdl' => WSDL_CACHE_NONE,
			'soap_version' => SOAP_1_2,
			'encoding' => 'UTF-8',
			'trace' => true);
	$client = new SoapClient($wsdluri, $soapConfig);
	$client->__setSoapHeaders( $authHeader );
	$handle = fopen($from_file, "rb");
	$options['data'] = base64_encode( fread($handle, filesize($from_file)) );
	$options['from_mtype'] = 'doc';
	$options['to_mtype']   = 'pdf';
	$options['return_by'] = 'soap';

	$res = $client->convert( $options );
	if($res['data']){
		$data = base64_decode($res['data']);
		$fp = fopen($to_file, "a");
		fputs($fp, $data);
	}
	var_dump($options, $res);die;
}catch(SoapFault $e){
	print $e;
}








/* Here, a sample of code wich use the Core_Request class. Require Zend_Config
 *
*
*
*/
/*
 set_include_path( APPLICATION_PATH.
 		PATH_SEPARATOR.APPLICATION_PATH. '/library'.
 		PATH_SEPARATOR.get_include_path() );

require_once ('Zend/Config/Ini.php');
require_once('Core/Request.php');

defined('APPLICATION_ENVIRONMENT')
or define('APPLICATION_ENVIRONMENT', 'production');

$configuration = new Zend_Config_Ini(
		APPLICATION_PATH . '/configs/client.ini',
		APPLICATION_ENVIRONMENT
);

if(ini_get('register_globals') == true){
print 'Before use RanchBE you must set Register_global php directive to off in php.ini file.
You can too set this directive in .htaccess file. See php documentation.';
die();
}

try {
$request = new Core_Request($from_file, 'pdf', $configuration);
$res = $request->test();
$res = $request->convert();
var_dump($res);
}catch(SoapFault $e){
print $e;
}
*/






