<?php
namespace ComposerScript;

use Composer\Script\Event;

/**
 *
 *
 */
class Installer
{

	public static function postUpdate(Event $event)
	{}

	public static function postInstall(Event $event)
	{
		// $installedPackage = $event->getOperation()->getPackage();
		// $composer = $event->getComposer();
		// copy('vendor/zendframework/zend-developer-tools/config/zenddevelopertools.local.php.dist','config/autoload/zdt.local.php');

		/* Twitter Bootstrap */
		if ( !is_dir('./server/public/bootstrap') ) {
			mkdir('./server/public/bootstrap');
		}
		if ( !is_dir('./server/public/bootstrap/js') ) {
			mkdir('./server/public/bootstrap/js');
		}
		if ( !is_dir('./server/public/bootstrap/css') ) {
			mkdir('./server/public/bootstrap/css');
		}
		if ( !is_dir('./server/public/bootstrap/fonts') ) {
			mkdir('./server/public/bootstrap/fonts');
		}

		$directory = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/css');
		foreach( $directory as $file ) {
			if ( $directory->isFile() ) {
				copy($directory->getPathname(), './server/public/bootstrap/css/' . $directory->getFileName());
			}
		}

		$directory = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/js');
		foreach( $directory as $file ) {
			if ( $directory->isFile() ) {
				copy($directory->getPathname(), './server/public/bootstrap/js/' . $directory->getFileName());
			}
		}

		$directory = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/fonts');
		foreach( $directory as $file ) {
			if ( $directory->isFile() ) {
				copy($directory->getPathname(), './server/public/bootstrap/fonts/' . $directory->getFileName());
			}
		}

		/* copy jquery in js */
		try {
			if ( !is_dir('./server/public/js/jquery') ) {
				mkdir('./server/public/js/jquery');
			}
			if ( !is_dir('./server/public/js/jqueryui') ) {
				mkdir('./server/public/js/jqueryui');
			}
			self::dircopy('vendor/components/jquery/', 'server/public/js/jquery', false);
			self::dircopy('vendor/components/jqueryui', 'server/public/js/jqueryui', false);
		}
		catch( \Exception $e ) {
			$package = 'Jquery or JqueryUi';
			echo sprintf('error during copy of %s, check if file is present', $package) . "\n\r";
			throw $e;
		}
	}

	/**
	 *
	 * @param Event $event
	 */
	public static function warmCache(Event $event)
	{
		// make cache toasty
	}

	/**
	 *
	 * @param string $src
	 * @param string $target
	 */
	public static function dircopy($src, $target, $verbose = true)
	{
		$directory = new \DirectoryIterator($src);
		foreach( $directory as $file ) {
			if ( $directory->isFile() ) {
				if ( $verbose ) echo 'copy ' . $directory->getPathname() . ' to ' . $target . '/' . $directory->getFileName() . "\n";
				copy($directory->getPathname(), $target . '/' . $directory->getFileName());
			}
			elseif ( $directory->getFileName() == '.git' ) {
				continue;
			}
			elseif ( $directory->isDir() && !$directory->isDot() ) {
				$dir = $directory->getPathname();
				$targetDir = $target . '/' . $directory->getFileName();
				if ( $verbose ) echo "directory " . $dir . " copy to $targetDir \n";
				if ( !is_dir($targetDir) ) {
					mkdir($targetDir);
				}
				self::dircopy($dir, $targetDir);
			}
		}
	}
}
