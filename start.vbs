Set objShell = CreateObject("WScript.Shell")
Set objEnv = objShell.Environment("User")

'first try to kill exisiting process
objShell.Run "stop.vbs"

'path to php
pathToPhp = "C:\rbconverter\bin\php-5.4.45-nts-Win32-VC9-x86"

'Set the new Path
objEnv("PATH") = objEnv("PATH") & ";" & pathToPhp
objShell.Run pathToPhp & "\rbconverter.exe -S 0.0.0.0:8888 -t ./server/public", 0, True
